﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;

namespace OnlineJewelleryShopping.Controllers
{
    public class HomeController : Controller
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        // GET: /Home/
        public ActionResult Category()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var aboutus = db.about_us.OrderByDescending(m=>m.about_us_id).Take(1).FirstOrDefault();
            return View(aboutus);
        }

        public ActionResult ContactUs()
        {
            var contact = db.contact_info.FirstOrDefault();
            ViewBag.add = contact.contact_info_address;
            ViewBag.phone = contact.contact_info_phone;
            Session["sendcontactus"] = null;
            return View();
        }
        [HttpPost]
        public ActionResult ContactUs(OnlineJewelleryShopping.Areas.Admin.Models.ContactUsModel contact_us,FormCollection collection)
        {

            var contact = db.contact_info.FirstOrDefault();
            ViewBag.add = contact.contact_info_address;
            ViewBag.phone = contact.contact_info_phone;

            //if(ModelState.IsValid)
            //{
            //    var insert=db.contact_us.Add(new c);
            //    //insert.contact_us_fullname = collection["contact_us_fullname"];
            //    //insert.contact_us_email=collection["contact_us_email"];
            //    //insert.contact_us_phone=collection["contact_us_phone"];
            //    //insert.contact_us_enquiry = collection["contact_us_enquiry"];
            //    //insert.contact_us_date = DateTime.Now;
            //    UpdateModel(insert);
            //    db.SaveChanges();
            //    Session["sendcontactus"] = "We have received your request, we will contact you in the shortest possible time.";
            //    return View();
            //}
            //return View(contact_us);


            //Check ModelState
            if (ModelState.IsValid) //Model is valid
            {
                //Add new contact 
                db.contact_us.Add(new contact_us()
                {
                    contact_us_phone = contact_us.contact_us_phone,
                    contact_us_fullname = contact_us.contact_us_fullname,
                    contact_us_enquiry = contact_us.contact_us_enquiry,
                    contact_us_email = contact_us.contact_us_email,
                    contact_us_date = DateTime.Now
                });
                //Save
                db.SaveChanges();
                //Redirect to index action
                return RedirectToAction("Index");
            }
            //ModelState is invalid
            //Return view and display error message
            return View(contact_us);
        }
        public ActionResult DeliveryInformation()
        {
            return View();
        }
        public ActionResult BuyVoucher()
        {
            return View();
        }
        public ActionResult Special()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult Lastest()
        {
            var product = db.products.OrderByDescending(m =>m.product_id).Take(4).ToList();
            return PartialView(product);
        }
        [ChildActionOnly]
        public ActionResult Featured()
        {
            var product = db.products.OrderByDescending(m => m.orders_detail.Count()).Take(4).ToList();
            return PartialView(product);
        }
        public ActionResult Parivacy()
        {
            var aboutus = db.term_and_conditional.OrderByDescending(m => m.term_and_conditional_id).Take(1).FirstOrDefault();
            return View(aboutus);
        }
    }
}
