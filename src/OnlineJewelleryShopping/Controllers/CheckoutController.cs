﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.ViewModel;

namespace OnlineJewelleryShopping.Controllers
{
    public class CheckoutController : Controller
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        //Ajax return custumer infomation
      
        //public ActionResult GetCusInfo(int id)
        //{
        //    return Json(new {ID = "" });
        //}
        // GET: /Checkout/

        public ActionResult Index()
        {

            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            var cart = ShoppingCart.GetCart(this.HttpContext);
            var cartiem = cart.GetCartItems();
            foreach (var item in cartiem)
            {
                int check = Convert.ToInt16(item.product_id);
                ViewBag.check = check;
            }
            int customerid=Convert.ToInt16(Session["customer_id"]);
            if(customerid != 0)
            {
                if (db.customers.Find(customerid) != null)
                {
                    var cust = db.customers.Where(m => m.customer_id == customerid).FirstOrDefault();
                    ViewBag.view1 = cust.customer_firstname == null ? "" : cust.customer_firstname;
                    ViewBag.view2 = cust.customer_lastname == null ? "" : cust.customer_lastname;
                    ViewBag.view3 = cust.customer_company == null ? "" : cust.customer_company;
                    ViewBag.view4 = cust.customer_address1 == null ? "" : cust.customer_address1;
                    ViewBag.view5 = cust.customer_address2 == null ? "" : cust.customer_address2;
                    ViewBag.view6 = cust.customer_city == null ? "" : cust.customer_city;
                    ViewBag.view7 = cust.customer_state == null ? "" : cust.customer_state;
                    ViewBag.view8 = cust.customer_zipcode == null ? "" : cust.customer_zipcode;
                    ViewBag.view9 = cust.customer_country == null ? "" : cust.customer_country;
                    ViewBag.view10 = cust.customer_phone == null ? "" : cust.customer_phone;
                }

            }
            return View();

        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {

            string billing_firstname = collection["txtfirstname"];
            string billing_lastname = collection["txtlastname"];
            string billing_company = collection["txtcompany"];
            string billing_address1 = collection["txtaddress1"];
            string billing_address2 = collection["txtaddress2"];
            string billing_city = collection["txtcity"];
            string billing_state = collection["txtstate"];
            string billing_zipcode = collection["txtzipcode"];
            string billing_country = collection["billing_country"];
            string billing_phone = collection["txtphone"];


            string shipping_firstname = collection["txtfirstname_ship"];
            string shipping_lastname = collection["txtlastname_ship"];
            string shipping_company = collection["txtcompany_ship"];
            string shipping_address1 = collection["txtaddress1_ship"];
            string shipping_address2 = collection["txtaddress2_ship"];
            string shipping_city = collection["txtcity_ship"];
            string shipping_state = collection["txtstate_ship"];
            string shipping_zipcode = collection["txtzipcode_ship"];
            string shipping_country = collection["shipping_country"];
            string shipping_phone = collection["txtphone_ship"];
            float order_total1 = float.Parse(collection["txttotal"]);
            float order_vat = float.Parse(collection["txtvat"]);
            string order_status = "Pending";
            string order_comment = collection["txtcomment"];
            string order_payment = collection["txtpayment"];
            DateTime order_recive = Convert.ToDateTime(collection["txtdatereceive"]);
            DateTime order_date = DateTime.Now;

            var addorders = new order()
            {
                customer_id = Convert.ToInt16(Session["customer_id"]),
                billing_firstname = billing_firstname,
                billing_lastname = billing_lastname,
                billing_company = billing_company,
                billing_address1 = billing_address1,
                billing_address2 = billing_address2,
                billing_city = billing_city,
                billing_state = billing_state,
                billing_zipcode = billing_zipcode,
                billing_country = billing_country,
                billing_phone = billing_phone,


                shipping_firstname = shipping_firstname,
                shipping_lastname = shipping_lastname,
                shipping_company = shipping_company,
                shipping_address1 = shipping_address1,
                shipping_address2 = shipping_address2,
                shipping_city = shipping_city,
                shipping_state = shipping_state,
                shipping_zipcode = shipping_zipcode,
                shipping_country = shipping_country,
                shipping_phone = shipping_phone,

                orders_comment = order_comment,
                orders_payment = order_payment,
                orders_total = order_total1,
                orders_vat = order_vat,
                orders_status = order_status,
                orders_date = order_date,
                orders_datereceive = order_recive
            };
            db.orders.Add(addorders);
            db.SaveChanges();

            var cart = ShoppingCart.GetCart(this.HttpContext);


            string shoppingid = Request.Cookies["cart"].Value;
            if (shoppingid != null)
            {
                var cartiem = cart.Getcartbyid(shoppingid);
                foreach (var item in cartiem)
                {
                    var product = db.products.First(m => m.product_id == item.product_id);
                    product.product_quantity = product.product_quantity - item.shopping_cart_count;
                    UpdateModel(product);
                }
                cart.CreateOrderByShopId(addorders, shoppingid);
            }
            else
            {
                var cartiem = cart.GetCartItems();
                foreach (var item in cartiem)
                {
                    var product = db.products.First(m => m.product_id == item.product_id);
                    product.product_quantity = product.product_quantity - item.shopping_cart_count;
                    UpdateModel(product);
                }
                cart.CreateOrder(addorders);
            }
            db.SaveChanges();

            Session["addcart"] = null;
            if (Request.Cookies["cart"] != null)
            {
                var user = new HttpCookie("cart")
                {
                    Expires = DateTime.Now.AddDays(-1),
                    Value = null
                };
                Response.Cookies.Add(user);
            }

            return RedirectToAction("OrderSuccess", "Checkout");
        }
        public ActionResult Cart()
        {
            var vat = db.account_tant.FirstOrDefault();
            ViewBag.vat = vat.account_tant_vat;
            var cart = ShoppingCart.GetCart(this.HttpContext);
            double total = 0;
            total = cart.GetTotal();

            string shoppingid = Request.Cookies["cart"].Value;
            if (Request.Cookies["cart"].Value != null)
            {
                if (total > 0)
                {
                    var viewModel = new ShoppingCartView()
                    {
                        CartItems = cart.Getcartbyid(shoppingid),
                        CartTotal = (float)cart.GetTotalbyshopid(shoppingid),
                        TotalOrder = (float)cart.TotalOrderbyshopid(shoppingid),
                        CartVAT = (float)cart.GetVatbyshopid(shoppingid)
                    };
                    return View(viewModel);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                {
                    if (total > 0)
                    {
                        var viewModel = new ShoppingCartView()
                        {
                            CartItems = cart.GetCartItems(),
                            CartTotal = (float)cart.GetTotal(),
                            TotalOrder = (float)cart.TotalOrder(),
                            CartVAT = (float)cart.GetVat()
                        };
                        return View(viewModel);
                    }
                    else
                    {
                        return View();
                    }
                }
            }
        }
        [HttpPost]
        public ActionResult AddToCart(FormCollection collection)
        {
            int quantity = Convert.ToInt16(collection["quantity"]);
            int id = Convert.ToInt16(collection["txtproduct_id"]);
            var addproduct = db.products.Single(m => m.product_id == id);
            var cart = ShoppingCart.GetCart(this.HttpContext);
            if (Request.Cookies.AllKeys.Contains("cart"))
            {
                string shoppingid = Request.Cookies["cart"].Value;
                cart.AddShoppingCartCookie(addproduct, shoppingid, quantity);
            }
            else
            {
                cart.AddShoppingCart(addproduct, quantity);
            }
            Session["addcart"] = addproduct.product_id;
            foreach (var item in cart.GetCartItems())
            {
                var Username = new HttpCookie("cart");
                Username.Value = item.shopping_cart_id;
                Username.Expires = DateTime.Now.AddDays(7);
                Response.Cookies.Add(Username);
            }


            return RedirectToAction("Cart", "Checkout");

        }

        [HttpPost]
        public ActionResult UpdateCart(int id, int count)
        {
            // Get the cart 
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of the album to display confirmation 
            string albumName = db.shopping_cart
                .Single(item => item.shopping_cart_recordid == id).product.product_name;
            int itemCount;
            // Update the cart count 
            if (Request.Cookies.AllKeys.Contains("cart"))
            {
                string shoppingid = Request.Cookies["cart"].Value;
                itemCount = cart.UpdateCartByShopid(id, shoppingid, count);

            }
            else
            {
                itemCount = cart.UpdateCart(id, count);
            }
            //Prepare messages
            string msg = "The quantity of " + Server.HtmlEncode(albumName) +
                    " has been refreshed in your shopping cart.";
            if (itemCount == 0) msg = Server.HtmlEncode(albumName) +
                    " has been removed from your shopping cart.";
            //
            // Display the confirmation message 
            var results = new ShoppingCartRemoveViewModel
            {
                Message = msg,
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
            return Json(results);

        }

        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of the album to display confirmation
            string albumName = db.shopping_cart
                .Single(item => item.shopping_cart_recordid == id).product.product_name;
            int itemCount;
            // Remove from cart
            if (Request.Cookies.AllKeys.Contains("cart"))
            {
                string shoppingid = Request.Cookies["cart"].Value;
                itemCount = cart.RemoveFormCartbyshopid(id, shoppingid);
            }
            else
            {
                itemCount = cart.RemoveFormCart(id);
            }
            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(albumName) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
            return Json(results);
        }
        [ChildActionOnly]
        public ActionResult OrderComfirm()
        {
            string shoppingid = Request.Cookies["cart"].Value;
            var vat = db.account_tant.FirstOrDefault();
            ViewBag.vat = vat.account_tant_vat;
            var cart = ShoppingCart.GetCart(this.HttpContext);
            double total = 0;
            total = cart.GetTotal();
            if (shoppingid != null)
            {
                if (total > 0)
                {
                    var viewModel = new ShoppingCartView()
                    {
                        CartItems = cart.Getcartbyid(shoppingid),
                        CartTotal = (float)cart.GetTotalbyshopid(shoppingid),
                        TotalOrder = (float)cart.TotalOrderbyshopid(shoppingid),
                        CartVAT = (float)cart.GetVatbyshopid(shoppingid)

                    };
                    return View(viewModel);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                if (total > 0)
                {
                    var viewModel = new ShoppingCartView()
                    {
                        CartItems = cart.GetCartItems(),
                        CartTotal = cart.GetTotal(),
                        TotalOrder = cart.TotalOrder(),
                        CartVAT = cart.GetVat()

                    };
                    return View(viewModel);
                }
                else
                {
                    return View();
                }
            }
        }
        public ActionResult OrderSuccess()
        {

            return View();
        }


    }
}
