﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using OnlineJewelleryShopping.Models;
using System.Web.UI;
using PagedList;

namespace OnlineJewelleryShopping.Controllers
{
    public class AccountController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        // GET: /Customer/Create

        public ActionResult Register()
        {
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            return View();
        }
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult IsEmailAvailable(string customer_email)
        {
            var emailcheck = db.customers.Where(m => m.customer_email == customer_email).Select(img => img.customer_email).FirstOrDefault();
            if (emailcheck == null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("<span style='color:Red;'> Emai in already in use</span>", JsonRequestBehavior.AllowGet);
            }
        }
        //
        // POST: /Customer/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(CustomerRegisterModel c, FormCollection collection)
        {
            string customer_email = collection["customer_email"];
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");

            if (ModelState.IsValid)
            {
                if (validemail(customer_email))
                {
                    if(c.Ischeck)
                        {

                    db.customers.Add(new customer()
                    {
                        customer_address1 = c.customer_address1,
                        customer_address2 = c.customer_address2,
                        customer_block = false,
                        customer_city = c.customer_city,
                        customer_company = c.customer_company,
                        customer_country = c.customer_country,
                        customer_date = DateTime.Now,
                        customer_email = c.customer_email,
                        customer_firstname = c.customer_firstname,
                        customer_lastname = c.customer_lastname,
                        customer_password = c.customer_password,
                        customer_phone = c.customer_phone,
                        customer_state = c.customer_state,
                        customer_zipcode = c.customer_zipcode
                    });
                    db.SaveChanges();

                    var customerid = db.customers.Where(m => m.customer_email == customer_email).FirstOrDefault();

                    Session["customer_id"] = customerid.customer_id;
                    Session["customer"] = customerid.customer_email;
                    Session["customer_firstname"] = c.customer_firstname;
                    Session["customer_lastname"] = c.customer_lastname;
                    return RedirectToAction("Index", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please select checkbox");

                    }

                }
                else
                {
                    ModelState.AddModelError("", "Email is already in use.");
                }

            }

            return View(c);
        }

        //

        public ActionResult Index()
        {
            if (Session["customer"] != null)
            {
                string email = Session["customer"].ToString();
                customer customer = db.customers.Where(u => u.customer_email == email).FirstOrDefault();
                return View(customer);
            }
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            int id = 0;
            ////Check cookies 
            if (Request.Cookies["emailcustomer"] != null )
            {
                        //Set session
                        Session["customer_id"] = id;
                        Session["customer"] = Request.Cookies["emailcustomer"].Value;
                        //Session["customer_firstname"] = Request.Cookies["Firstname"].Value;
                        //Session["customer_lastname"] = Request.Cookies["Lastname"].Value;
                        Session["IsAuthorized"] = true;
                       //Go te admin manager page
                        return RedirectToAction("Index");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(OnlineJewelleryShopping.Models.LoginModel userr)
        {

            if (ModelState.IsValid)
            {
                if (IsValid(userr.customer_email, userr.customer_password))
                {
                    if (checkblock(userr.customer_email))
                    {
                        FormsAuthentication.SetAuthCookie(userr.customer_email, false);
                        var customerid = db.customers.Where(m => m.customer_email.ToUpper() == userr.customer_email.ToUpper()).FirstOrDefault();
                        Session["customer_id"] = customerid.customer_id;
                        Session["customer"] = userr.customer_email;
                        Session["customer_firstname"] = customerid.customer_firstname;
                        Session["customer_lastname"] = customerid.customer_lastname;
                        if(userr.Remember)
                        {
                            Response.Cookies.Add(new HttpCookie("Firstname", customerid.customer_firstname));
                            Response.Cookies.Add(new HttpCookie("Lastname", customerid.customer_lastname));
                            Response.Cookies.Add(new HttpCookie("IsAuthorized", "true"));

                            var Username = new HttpCookie("customerid");
                            Username.Value = customerid.customer_id.ToString();
                            Username.Expires = DateTime.Now.AddDays(7);
                            Response.Cookies.Add(Username);
                            var Username1 = new HttpCookie("emailcustomer");
                            Username.Value = customerid.customer_firstname;
                            Username.Expires = DateTime.Now.AddDays(7);
                            Response.Cookies.Add(Username1);

                        }
                        return RedirectToAction("Index", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Your account has been locked. Please contact shop.");

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Login details are wrong.");
                }
                return View(userr);
            }
            return View(userr);
        }
        public bool validemail(string customer_email)
        {
            bool emailcheck = true;
            using (var db = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                var customer = db.customers.FirstOrDefault(m => m.customer_email == customer_email);
                if (customer != null)
                {
                    emailcheck = false;
                }
            }
            return emailcheck;
        }
        public bool checkblock(string email)
        {
            bool checkblock = true;
            var checkblockuser = db.customers.First(m => m.customer_email == email);
            if (checkblockuser.customer_block == true)
            {
                checkblock = false;
            }

            return checkblock;
        }
        public bool IsValid(string customer_email, string customer_password)
        {
            var crypyo = new SimpleCrypto.PBKDF2();
            bool IsValid = false;
            using (var db = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                var user = db.customers.FirstOrDefault(m => m.customer_email == customer_email);
                if (user != null)
                {
                    if (user.customer_password == customer_password)
                    {
                        IsValid = true;
                    }
                }
            }
            return IsValid;
        }



        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            Session["customer_id"] = null;
            Session["customer"] = null;
            Session["customer_firstname"] = null;
            Session["customer_lastname"] = null;
            if (Request.Cookies["customerid"] != null)
            {
                HttpCookie myCookie = new HttpCookie("customerid");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["emailcustomer"] != null)
            {
                HttpCookie myCookie = new HttpCookie("emailcustomer");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["Firstname"] != null)
            {
                HttpCookie myCookie = new HttpCookie("Firstname");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["Lastname"] != null)
            {
                HttpCookie myCookie = new HttpCookie("Lastname");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);

            }
            if (Request.Cookies["IsAuthorized"] != null)
            {
                HttpCookie myCookie = new HttpCookie("IsAuthorized");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            return RedirectToAction("Index", "Home");
        }
        public ActionResult Edit(int id = 0)
        {
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        public bool validemailedit(string customer_email)
        {
            int customer_id = Convert.ToInt16(Session["customer_id"]);
            bool emailcheck = true;
            using (var db = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                var customer = db.customers.FirstOrDefault(m => m.customer_id == customer_id);
                if (customer.customer_email == customer_email)
                {
                    emailcheck = true;
                }
                else
                {
                    var checkemailother = db.customers.FirstOrDefault(m => m.customer_email == customer_email);
                    if (checkemailother != null)
                    {
                        emailcheck = false;
                    }
                    else
                    {
                        emailcheck = true;
                    }
                }
            }
            return emailcheck;
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {

            if (ModelState.IsValid)
            {

                string customer_firstname = collection["customer_firstname"];
                string customer_lastname = collection["customer_lastname"];
                string customer_email = collection["customer_email"];
                string customer_phone = collection["customer_phone"];
                if (validemailedit(customer_email))
                {
                    var customer = db.customers.First(m => m.customer_id == id);

                    customer.customer_id = id;
                    customer.customer_firstname = customer_firstname;
                    customer.customer_lastname = customer_lastname;
                    customer.customer_email = customer_email;
                    customer.customer_phone = customer_phone;
                    UpdateModel(customer);
                    db.SaveChanges();

                    var customerid = db.customers.Where(m => m.customer_email == customer_email).FirstOrDefault();
                    Session["customer_id"] = customerid.customer_id;
                    Session["customer"] = customer_email;
                    Session["customer_firstname"] = customerid.customer_firstname;
                    Session["customer_lastname"] = customerid.customer_lastname;
                    return RedirectToAction("Edit", "Account");
                }
                else
                {

                    ModelState.AddModelError("", "Email is already in use.");
                }
            }
            customer showinfo = db.customers.Find(id);
            if (showinfo == null)
            {
                return HttpNotFound();
            }
            return View(showinfo);

        }

        public ActionResult Address()
        {
            if (Session["customer"] != null)
            {
                string email = Session["customer"].ToString();
                customer customer = db.customers.Where(u => u.customer_email == email).FirstOrDefault();
                return View(customer);
            }
            return View();
        }
        public ActionResult EditAddress()
        {

            if (Session["customer"] != null)
            {
                string email = Session["customer"].ToString();
                customer customer = db.customers.Where(u => u.customer_email == email).FirstOrDefault();
                ViewBag.country = CountryList.GetListcountry(customer.customer_country);
                return View(customer);
            }

            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAddress(int id, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                var customer = db.customers.First(m => m.customer_id == id);
                string customer_firstname = collection["customer_firstname"];
                string customer_lastname = collection["customer_lastname"];
                string customer_company = collection["customer_company"];
                string customer_address1 = collection["customer_address1"];
                string customer_address2 = collection["customer_address2"];
                string customer_city = collection["customer_city"];
                string customer_zipcode = collection["customer_zipcode"];
                string customer_state = collection["customer_state"];
                string customer_country = collection["customer_country"];

                customer.customer_id = id;
                customer.customer_firstname = customer_firstname;
                customer.customer_lastname = customer_lastname;
                customer.customer_company = customer_company;
                customer.customer_address1 = customer_address1;
                customer.customer_address2 = customer_address2;
                customer.customer_city = customer_city;
                customer.customer_zipcode = customer_zipcode;
                customer.customer_state = customer_state;
                customer.customer_country = customer_country;

                UpdateModel(customer);
                db.SaveChanges();
                Session["message"] = "You have change address success.";
                return RedirectToAction("Address", "Account");
            }

            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            return View();
        }

        public ActionResult ChangePwd()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePwd(CustomerChangePasswordModel customer, FormCollection collection)
        {

            int customer_id = Convert.ToInt16(Session["customer_id"]);
            if (ModelState.IsValid)
            {
                var customer1 = db.customers.First(m => m.customer_id == customer_id);
                string customer_password = collection["customer_password_confirm"];
                string customer_password_old = collection["customer_password_old"];
                if (customer1.customer_password == customer_password_old)
                {
                    customer1.customer_id = customer_id;
                    customer1.customer_password = customer_password;
                    UpdateModel(customer1);
                    db.SaveChanges();
                    Session["message"] = "You have change password success.";
                    return RedirectToAction("ChangePwd", "Account");
                }
                else
                {

                    ModelState.AddModelError("", "Current password is incorrect.");
                }
            }
            return View();
        }
        public ActionResult Forget_Pwd()
        {
            return View();
        }
        public ActionResult WishList()
        {
            return View();
        }
        public ActionResult OrderHistory(int? page)
        {

            int pagesize = 5;
            int pagenumber = (page ?? 1);

            int customer_id = Convert.ToInt16(Session["customer_id"]);
            var customer = db.orders.OrderByDescending(m=>m.orders_id).Where(m => m.customer_id == customer_id).ToList();


            return View(customer.ToPagedList(pagenumber, pagesize));
        }
        public ActionResult OrderDetails(int id)
        {
            int customer_id = Convert.ToInt16(Session["customer_id"]);
            var orders = db.orders.Include("orders_detail").OrderByDescending(m => m.orders_id).Where(m => m.orders_id == id).ToList();
            return View(orders);


        }
        public ActionResult OrderReturn()
        {
            return View();
        }
        public ActionResult OrderTrans()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult Menu()
        {
            if (Session["customer"] != null)
            {
                string email = Session["customer"].ToString();

                customer customer = db.customers.Where(u => u.customer_email == email).FirstOrDefault();
                return PartialView(customer);

            }
            return PartialView();
        }


    }
}
