﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.ViewModel;

namespace OnlineJewelleryShopping.Controllers
{
    public class SiteController : Controller
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        // GET: /Site/

        public ActionResult Index()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult Top()
        {

            var cart = ShoppingCart.GetCart(this.HttpContext);
            if (Request.Cookies.AllKeys.Contains("cart"))
            {
                string shoppingid = Request.Cookies["cart"].Value;
                var viewModel = new ShoppingCartView()
                {
                    CartItems = cart.Getcartbyid(shoppingid)

                };
                var cartiem = cart.Getcartbyid(shoppingid);
                foreach (var item in cartiem)
                {
                    int check = Convert.ToInt16(item.product_id);
                    ViewBag.check = check;
                }
                return PartialView(viewModel);
            }
            else
            {

                var viewModel = new ShoppingCartView()
                {
                    CartItems = cart.GetCartItems()

                };
                var cartiem = cart.GetCartItems();
                foreach (var item in cartiem)
                {
                    int check = Convert.ToInt16(item.product_id);
                    ViewBag.check = check;
                }
                return PartialView(viewModel);
            }


        }
        [ChildActionOnly]
        public ActionResult Advertising()
        {
            var Advertising = db.advertises.Take(1).ToList();
            return PartialView(Advertising);
        }
        [ChildActionOnly]
        public ActionResult Advproduct()
        {
            var Advertising = db.advertises.Take(1).ToList();
            return PartialView(Advertising);
        }

        [ChildActionOnly]
        public ActionResult Advertisingcarouse()
        {
            var Advertising = db.advertises.Take(11).ToList();
            return PartialView(Advertising);
        }

        [ChildActionOnly]
        public ActionResult Banner()
        {
            var banner = db.banners.OrderByDescending(m => m.banner_id).Take(5).ToList();
            return PartialView(banner);
        }

        [ChildActionOnly]
        public ActionResult Footer()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult Storeinfo()
        {
            var aboutinfo = db.contact_info.Take(1).FirstOrDefault();
            return PartialView(aboutinfo);
        }

        [ChildActionOnly]
        public ActionResult Recently()
        {
            return PartialView();
        }
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);
            if (Request.Cookies.AllKeys.Contains("cart"))
            {
                string shoppingid = Request.Cookies["cart"].Value;
                ViewData["CartCount"] = cart.GetCountbyshopid(shoppingid);
                ViewData["CartTotal"] = float.Parse(cart.GetTotalbyshopid(shoppingid).ToString());
            }
            else
            {
                ViewData["CartCount"] = cart.GetCount();
                ViewData["CartTotal"] = float.Parse(cart.GetTotal().ToString());
            }
            return PartialView("CartSummary");
        }

        [ChildActionOnly]
        public ActionResult Search()
        {
            return PartialView();
        }



    }
}
