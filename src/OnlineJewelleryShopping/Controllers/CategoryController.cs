﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using OnlineJewelleryShopping.Models;

namespace OnlineJewelleryShopping.Controllers
{
    public class CategoryController : Controller
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        // GET: /Category/

        public ActionResult Index(int id)
        {
            var category = db.categories.Where(m => m.category_id == id).FirstOrDefault();
            ViewBag.Title = category.category_name;
            return View();
        }
        [ChildActionOnly]
        public ActionResult Show()
        {
            var category = db.categories.OrderByDescending(m => m.category_id).Where(m => m.category_parent == null);
            foreach (var item in category)
            {
                db.categories.OrderByDescending(m => m.category_id == m.category_parent).ToList();

            }
            return View(category);

        }
        [ChildActionOnly]
        public ActionResult Menu(int id)
        {
            ViewBag.categoryid = id;
            var category = db.categories.Where(m => m.category_parent == null);
            foreach (var item in category)
            {
                db.categories.Where(m => m.category_id == m.category_parent).ToList();
            }
            return View(category);

        }
        [ChildActionOnly]
        public ActionResult MenuPrice()
        {
            var category = db.categories.Where(m => m.category_parent == null);
            foreach (var item in category)
            {
                db.categories.Where(m => m.category_id == m.category_parent).ToList();
            }
            return View(category);

        }
        [ChildActionOnly]
        public ActionResult Conten(int id, int page = 1, int PageSize = 15, string Order = "")
        {
            int pagesize = PageSize;
            int pagenumber = page;

            //  var categorys = db.products.Where(m => m.category_id == id).OrderByDescending(m=>m.product_name);
            //var childrenCate = from i in db.categories where i.category_parent == id select i.category_id; //Get list child cate id
            var categorys = from i in db.products
                            where
                                i.category_id == id ||
                                (from i2 in db.categories where i2.category_parent == id && i2.category_id == i.category_id select i2).Count() > 0
                            select i;
            //Order Module
            switch (Order)
            {
                case "Name_asc":
                    categorys = from i in categorys orderby i.product_name ascending select i;
                    break;
                case "Name_desc":
                    categorys = from i in categorys orderby i.product_name descending select i;
                    break;
                case "Price_asc":
                    categorys = from i in categorys orderby i.product_price ascending select i;
                    break;
                case "Price_desc":
                    categorys = from i in categorys orderby i.product_price descending select i;
                    break;
                default:
                    categorys = from i in categorys orderby i.product_id descending select i;
                    break;
            }
            ViewBag.CurrentOrder = Order;
            ViewBag.categoryid = id;
            ViewBag.CurrentPageSize = PageSize;
            return PartialView(categorys.ToPagedList(pagenumber, pagesize));
        }
        [ChildActionOnly]
        public ActionResult Featured()
        {
            var product = db.products.OrderByDescending(m => m.orders_detail.Count()).Take(4).ToList();

            return PartialView(product);
        }
        [ChildActionOnly]
        public ActionResult InfoCategory(int id)
        {
            var categorys = db.categories.Where(m => m.category_id == id).FirstOrDefault();
            return PartialView(categorys);
        }

    }
}
