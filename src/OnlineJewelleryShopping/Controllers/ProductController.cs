﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;
using PagedList;
namespace OnlineJewelleryShopping.Controllers
{
    public class ProductController : Controller
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        //
        // GET: /Product/

        public ActionResult Index(int id)
        {
            float discounts = 0;
            var products = db.products.Include("product_discount_detail").Where(m => m.product_id == id).ToList();
            foreach (var dis in products)
            {

                foreach (var item in dis.product_discount_detail)
                {
                    discounts = (float)dis.product_price * (float)item.product_discount.product_discount_amount / 100;
                    ViewBag.saveyou = (float)dis.product_price - discounts;
                    ViewBag.per = item.product_discount.product_discount_amount;
                    ViewBag.Title = dis.product_name;
                }

            }

            return PartialView(products);
        }
        [ChildActionOnly]
        public ActionResult Menuleft()
        {

            return PartialView();
        }
        [ChildActionOnly]
        public ActionResult pricerangeshow()
        {

            return PartialView();
        }
        public object PriceRange(int? page, string from, string to, int PageSize = 15, string Order = "")
        {

            int pagesize = PageSize;
            int pagenumber = (page ?? 1);
            int pricefrom = Convert.ToInt32(from);
            int priceto = Convert.ToInt32(to);
            ViewBag.from = pricefrom;
            ViewBag.to = priceto;
            ViewBag.CurrentPageSize = PageSize;
            ViewBag.CurrentOrder = Order;
            var product = from m in db.products where m.product_price >= pricefrom && m.product_price <= priceto select m;

            //Order Module
            switch (Order)
            {
                case "Name_asc":
                    product = from i in product orderby i.product_name ascending select i;
                    break;
                case "Name_desc":
                    product = from i in product orderby i.product_name descending select i;
                    break;
                case "Price_asc":
                    product = from i in product orderby i.product_price ascending select i;
                    break;
                case "Price_desc":
                    product = from i in product orderby i.product_price descending select i;
                    break;
                default:
                    product = from i in product orderby i.product_id descending select i;
                    break;
            }
            return View(product.ToPagedList(pagenumber, pagesize));
        }
        public object SearchByName(string Keyword = "", int Pagenumber = 1, int PageSize = 10, string Brand = "0", string Category = "0", string Quantity = "All", string sub_category = "", string pricefrom = "", string priceto = "", string sub_price = "", string order = "asc")
        {
            //Data to view 
            //Current order
            ViewBag.CurrentOrder = order;
            //Current Key word
            ViewBag.name = Keyword;
            ViewBag.pagesize = PageSize;
            //Current Brand
            ViewBag.Brandcurrent = Brand;
            //Current Category
            ViewBag.Categorycurrent = Category;

            //Current Quantity

            //Current Status

            //Brand
            ViewBag.Brand = BrandModel.GetListBrands();

            //Category
            ViewBag.Category = CategoryModel.Get2Level();

            //All product
            var lstObj = from c in db.products select c;

            //Search module
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }


            //Brand filter
            if (Brand != null && Brand != "0" && Brand !="")
            {
                int id = int.Parse(Brand);
                lstObj = from i in lstObj where i.brand_id == id select i;
            }

            //Category filter
            if (Category != null && Category != "0" &&Category !="")
            {
                int id = int.Parse(Category);
                lstObj = from i in lstObj where i.category_id == id select i;
            }
            //switch (Quantity)
            //{
            //    case "All":
            //        break;
            //    case "Available":
            //        lstObj = from i in lstObj where i.product_quantity > 0 select i;
            //        break;
            //    case "SoldOut":
            //        lstObj = from i in lstObj where i.product_quantity == 0 select i;
            //        break;

            //    default:
            //        break;
            //}
            switch (order)
            {
                case "default":
                    lstObj = from i in lstObj orderby i.product_id descending select i;
                    break;
                case "desc":
                    lstObj = from i in lstObj orderby i.product_price descending select i;
                    break;
                case "asc":
                    lstObj = from i in lstObj orderby i.product_price ascending select i;
                    break;
                case "namea":
                    lstObj = from i in lstObj orderby i.product_name ascending select i;
                    break;
                case "namez":
                    lstObj = from i in lstObj orderby i.product_name descending select i;
                    break;
                default:
                    lstObj.OrderBy(m => m.product_price);
                    break;
            }
            //lstObj = from i in lstObj orderby i.product_id descending select i;

            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }


        public ActionResult Review(int id, int? page)
        {
            int pagesize = 10;
            int pagenumber = (page ?? 1);
            ViewBag.product_id = id;
            var review = db.product_review.Where(m => m.product_id == id).OrderByDescending(m => m.product_review_date).ToList();
            return PartialView(review.ToPagedList(pagenumber, pagesize));
        }

        [HttpPost]
        public ActionResult Review(int id, int? page, FormCollection collection)
        {

            int pagesize = 10;
            int pagenumber = (page ?? 1);

            string fulname = collection["fullname"];
            string content = collection["txtcontent"];
            int product_id = Convert.ToInt16(collection["txtproduct_id"]);

            ViewBag.product_id = id;
            bool status = false;
            var insert = new product_review()
            {
                product_id = product_id,
                product_review_name = fulname,
                product_review_text = content,
                product_review_status = status,
                product_review_date = DateTime.Now,
            };
            db.product_review.Add(insert);
            db.SaveChanges();
            var review = db.product_review.Where(m => m.product_id == id).ToList();
            return PartialView(review.ToPagedList(pagenumber, pagesize));
        }
        public ActionResult Brand(int id, int? page)
        {
            int pagesize = 10;
            int pagenumber = (page ?? 1);
            var brand = db.products.Where(m => m.brand_id == id).ToList();
            foreach (var item in brand)
            {
                ViewBag.Title = item.brand.brand_name;
            }
            return View(brand.ToPagedList(pagenumber, pagesize));
        }

    }
}
