﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;

namespace OnlineJewelleryShopping.ViewModel
{
    public class ShoppingCartView
    {
        public List<shopping_cart> CartItems { get; set; }
        public double CartTotal { get; set; }
        public double TotalOrder { get; set; }
        public double CartVAT { get; set; }
    }
}
