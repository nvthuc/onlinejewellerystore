﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Models
{
    public partial class ShoppingCart
    {
        OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();
        string ShoppingCartId { get; set; }
        public const string CartSessionKey = "shopping_cart_id";
        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }
        public void AddShoppingCart(product product, int count)
        {
            var cartitem = db.shopping_cart.SingleOrDefault(m => m.shopping_cart_id == ShoppingCartId && m.product_id == product.product_id);


            if (cartitem == null)
            {
                cartitem = new shopping_cart()
                {
                    product_id = product.product_id,
                    shopping_cart_id = ShoppingCartId,
                    shopping_cart_count = count,
                    shopping_cart_date = DateTime.Now
                };
                db.shopping_cart.Add(cartitem);

            }
            else
            {
                cartitem.shopping_cart_count++;
            }

            db.SaveChanges();
        }
        public void AddShoppingCartCookie(product product,string shoppingid, int count)
        {
            var cartitem = db.shopping_cart.SingleOrDefault(m => m.shopping_cart_id == shoppingid && m.product_id == product.product_id);


            if (cartitem == null)
            {
                cartitem = new shopping_cart()
                {
                    product_id = product.product_id,
                    shopping_cart_id = shoppingid,
                    shopping_cart_count = count,
                    shopping_cart_date = DateTime.Now
                };
                db.shopping_cart.Add(cartitem);

            }
            else
            {
                cartitem.shopping_cart_count = cartitem.shopping_cart_count + count;
            }

            db.SaveChanges();
        }
        public int RemoveFormCart(int id)
        {
            var cartitem = db.shopping_cart.Single(m => m.shopping_cart_id == ShoppingCartId && m.shopping_cart_recordid == id);
            int itemcount = 0;
            if (cartitem != null)
            {
                db.shopping_cart.Remove(cartitem);

                db.SaveChanges();
            }
            return itemcount;
        }
        public int RemoveFormCartbyshopid(int id,string shopid)
        {
            var cartitem = db.shopping_cart.Single(m => m.shopping_cart_id == shopid && m.shopping_cart_recordid == id);
            int itemcount = 0;
            if (cartitem != null)
            {
                db.shopping_cart.Remove(cartitem);

                db.SaveChanges();
            }
            return itemcount;
        }
        public void Emtycart()
        {

            var cartItems = db.shopping_cart.Where(
                cart => cart.shopping_cart_id == ShoppingCartId);

            foreach (var cartItem in cartItems)
            {
                db.shopping_cart.Remove(cartItem);
            }
            // Save changes
            db.SaveChanges();
        }
        public void Emtycartbyid(string shopid)
        {

            var cartItems = db.shopping_cart.Where(
                cart => cart.shopping_cart_id == shopid);

            foreach (var cartItem in cartItems)
            {
                db.shopping_cart.Remove(cartItem);
            }
            // Save changes
            db.SaveChanges();
        }
        public int UpdateCart(int id, int count)
        {
            var cartitem = db.shopping_cart.Single(m => m.shopping_cart_id == ShoppingCartId && m.shopping_cart_recordid == id);
            int itemcount = 0;
            if (cartitem.product.product_quantity > count)
            {
                if (cartitem != null)
                {
                    if (count > 0)
                    {
                        cartitem.shopping_cart_count = count;
                        itemcount = (int)cartitem.shopping_cart_count;

                    }
                    else
                    {
                        db.shopping_cart.Remove(cartitem);
                    }
                    db.SaveChanges();
                }
            }
            return itemcount;
        }
        public int UpdateCartByShopid(int id,string shopid, int count)
        {
            var cartitem = db.shopping_cart.Single(m => m.shopping_cart_id == shopid && m.shopping_cart_recordid == id);
            int itemcount = 0;
            if (cartitem.product.product_quantity > count)
            {
                if (cartitem != null)
                {
                    if (count > 0)
                    {
                        cartitem.shopping_cart_count = count;
                        itemcount = (int)cartitem.shopping_cart_count;

                    }
                    else
                    {
                        db.shopping_cart.Remove(cartitem);
                    }
                    db.SaveChanges();
                }
            }
            return itemcount;
        }
        public int GetCount()
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in db.shopping_cart
                          where cartItems.shopping_cart_id == ShoppingCartId
                          select (int?)cartItems.shopping_cart_count).Sum();
            // Return 0 if all entries are null
            return count ?? 0;
        }
        public int GetCountbyshopid(string shopid)
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in db.shopping_cart
                          where cartItems.shopping_cart_id == shopid
                          select (int?)cartItems.shopping_cart_count).Sum();
            // Return 0 if all entries are null
            return count ?? 0;
        }
        public List<shopping_cart> GetCartItems()
        {
            return db.shopping_cart.Where(m => m.shopping_cart_id == ShoppingCartId).ToList();

        }
        public List<shopping_cart> Getcartbyid(string id)
        {
            return db.shopping_cart.Where(m => m.shopping_cart_id == id).ToList();

        }
        public double GetVat()
        {
            float total;
            var vat = db.account_tant.FirstOrDefault();
            total = float.Parse(GetTotal().ToString()) * float.Parse(vat.account_tant_vat.ToString()) / 100;

            return total;
        }
        public double GetVatbyshopid(string shopid)
        {
            float total;
            var vat = db.account_tant.FirstOrDefault();
            total = float.Parse(GetTotalbyshopid(shopid).ToString()) * float.Parse(vat.account_tant_vat.ToString()) / 100;

            return total;
        }
        public double TotalOrder()
        {
            double total = 0;
            total = GetTotal() + GetVat();
            return total;
        }
        public double TotalOrderbyshopid(string shopid)
        {
            double total = 0;
            total = GetTotalbyshopid(shopid) + GetVatbyshopid(shopid);
            return total;
        }
        public double GetTotal()
        {

            float? total = (float?)(from cartItems in db.shopping_cart
                                    where cartItems.shopping_cart_id == ShoppingCartId
                                    select (int?)cartItems.shopping_cart_count *
                                    cartItems.product.product_price).Sum();
            float discounts = 0;
            var shopp = db.shopping_cart.Where(m => m.shopping_cart_id == ShoppingCartId).ToList();
            foreach (var item in shopp)
            {
                var products = db.products.Include("product_discount_detail").Where(m => m.product_id == item.product_id).ToList();
                foreach (var dis in products)
                {
                    foreach (var getdis in dis.product_discount_detail)
                    {
                        if (getdis.product_discount.product_discount_startdate<= DateTime.Now && getdis.product_discount.product_discount_enddate>= DateTime.Now)
                        {
                            discounts = (float)dis.product_price * (int)getdis.product_discount.product_discount_amount / 100;
                        }
                        else
                        {
                            discounts = 0;
                        }
                    }

                }
            }
            if (discounts > 0)
            {
                float int2 = (float)total - (float)discounts;
                return int2;
            }
            else
            {
                return total ?? double.Epsilon;
            }

        }
        public double GetTotalbyshopid(string shopid)
        {

            float? total = (float?)(from cartItems in db.shopping_cart
                                    where cartItems.shopping_cart_id == shopid
                                    select (int?)cartItems.shopping_cart_count *
                                    cartItems.product.product_price).Sum();
            float discounts = 0;
            var shopp = db.shopping_cart.Where(m => m.shopping_cart_id == shopid).ToList();
            foreach (var item in shopp)
            {
                var products = db.products.Include("product_discount_detail").Where(m => m.product_id == item.product_id).ToList();
                foreach (var dis in products)
                {
                    foreach (var getdis in dis.product_discount_detail)
                    {
                        if (getdis.product_discount.product_discount_startdate <= DateTime.Now && getdis.product_discount.product_discount_enddate >= DateTime.Now)
                        {
                            discounts = (float)dis.product_price * (int)getdis.product_discount.product_discount_amount / 100;
                        }
                        else
                        {
                            discounts = 0;
                        }
                    }

                }
            }
            if (discounts > 0)
            {
                float int2 = (float)total - (float)discounts;
                return int2;
            }
            else
            {
                return total ?? double.Epsilon;
            }

        }
        public float GetDis()
        {
            float discounts = 0;
            var shopp = db.shopping_cart.Where(m => m.shopping_cart_id == ShoppingCartId).ToList();
            foreach (var item in shopp)
            {
                var products = db.products.Include("product_discount_detail").Where(m => m.product_id == item.product_id).ToList();
                foreach (var dis in products)
                {
                    foreach (var getdis in dis.product_discount_detail)
                    {
                        if (getdis.product_discount.product_discount_startdate<= DateTime.Now && getdis.product_discount.product_discount_enddate >= DateTime.Now)
                        {
                            discounts = (float)dis.product_price * (float)getdis.product_discount.product_discount_amount / 100;
                        }
                    }

                }
            }
            return discounts;
        }

        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] = context.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();

                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }

            return context.Session[CartSessionKey].ToString();
        }
        public int CreateOrder(order order)
        {
            float ordertotal = 0;
            var cartiem = GetCartItems();
            foreach (var item in cartiem)
            {
                var discount = db.product_discount_detail.Where(m => m.product_id == item.product_id).FirstOrDefault();
                if (item.product.product_discount_detail.Count > 0)
                {
                    if (discount.product_discount.product_discount_startdate<= DateTime.Now && discount.product_discount.product_discount_enddate >= DateTime.Now)
                    {
                        var orderdetail = new orders_detail()
                        {
                            product_id = (int)item.product_id,
                            orders_id = order.orders_id,
                            orders_detail_price = (double)item.product.product_price,
                            orders_detail_quantity = item.shopping_cart_count,
                            orders_detail_discount = (double)item.product.product_price * discount.product_discount.product_discount_amount / 100

                        };
                        db.orders_detail.Add(orderdetail);
                    }
                    else
                    {

                        var orderdetail = new orders_detail()
                        {
                            product_id = (int)item.product_id,
                            orders_id = order.orders_id,
                            orders_detail_price = (double)item.product.product_price,
                            orders_detail_quantity = item.shopping_cart_count,
                            orders_detail_discount = 0

                        };
                        db.orders_detail.Add(orderdetail);
                    }
                }
                else
                {
                    var orderdetail = new orders_detail()
                    {
                        product_id = (int)item.product_id,
                        orders_id = order.orders_id,
                        orders_detail_price = (double)item.product.product_price,
                        orders_detail_quantity = item.shopping_cart_count,
                        orders_detail_discount = 0

                    };
                    db.orders_detail.Add(orderdetail);
                }
                ordertotal = (float)TotalOrder();

            }
            order.orders_total = (double)ordertotal;


            Emtycart();

            return order.orders_id;
        }
        public int CreateOrderByShopId(order order,string shopid)
        {
            float ordertotal = 0;
            var cartiem = Getcartbyid(shopid);
            foreach (var item in cartiem)
            {
                var discount = db.product_discount_detail.Where(m => m.product_id == item.product_id).FirstOrDefault();
                if (item.product.product_discount_detail.Count > 0)
                {
                    if (discount.product_discount.product_discount_startdate <= DateTime.Now && discount.product_discount.product_discount_enddate >= DateTime.Now)
                    {
                        var orderdetail = new orders_detail()
                        {
                            product_id = (int)item.product_id,
                            orders_id = order.orders_id,
                            orders_detail_price = (double)item.product.product_price,
                            orders_detail_quantity = item.shopping_cart_count,
                            orders_detail_discount = (double)item.product.product_price * discount.product_discount.product_discount_amount / 100

                        };
                        db.orders_detail.Add(orderdetail);
                    }
                    else
                    {

                        var orderdetail = new orders_detail()
                        {
                            product_id = (int)item.product_id,
                            orders_id = order.orders_id,
                            orders_detail_price = (double)item.product.product_price,
                            orders_detail_quantity = item.shopping_cart_count,
                            orders_detail_discount = 0

                        };
                        db.orders_detail.Add(orderdetail);
                    }
                }
                else
                {
                    var orderdetail = new orders_detail()
                    {
                        product_id = (int)item.product_id,
                        orders_id = order.orders_id,
                        orders_detail_price = (double)item.product.product_price,
                        orders_detail_quantity = item.shopping_cart_count,
                        orders_detail_discount = 0

                    };
                    db.orders_detail.Add(orderdetail);
                }
                ordertotal = (float)TotalOrderbyshopid(shopid);

            }
            order.orders_total = (double)ordertotal;


            Emtycartbyid(shopid);

            return order.orders_id;
        }

    }
}
