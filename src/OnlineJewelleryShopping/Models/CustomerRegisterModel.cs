﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Models
{
    public class CustomerRegisterModel
    {
        public int customer_id { get; set; }
        [Required]
        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 3)]
        public string customer_firstname { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 3)]
        public string customer_lastname { get; set; }

        public string customer_company { get; set; }
        [Required]
        [Display(Name = "Address1")]
        [StringLength(100, MinimumLength = 3)]
        public string customer_address1 { get; set; }

        public string customer_address2 { get; set; }
        [Required]
        [Display(Name = "City")]
        [StringLength(100, MinimumLength = 3)]
        public string customer_city { get; set; }
        [Required]
        [Display(Name = "Zipcode")]
        [StringLength(100, MinimumLength = 3)]
        public string customer_zipcode { get; set; }

        [Required]
        [Display(Name = "State")]
        [StringLength(100, MinimumLength = 2)]
        public string customer_state { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string customer_country { get; set; }
        [Required]
        [Display(Name = "Phone")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string customer_phone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [StringLength(50, MinimumLength = 3)]
        public string customer_email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(50, MinimumLength = 6)]
        public string customer_password { get; set; }

        [Required]
        [Display(Name = "Password Comfirm")]
        [StringLength(50, MinimumLength = 6)]
        [Compare("customer_password", ErrorMessage = "The password and confirmation password do not match.")]
        public string customer_password_confirm { get; set; }
        public Nullable<bool> customer_block { get; set; }
        public Nullable<System.DateTime> customer_date { get; set; }
        public bool Ischeck { get; set; }
    }
    public class CustomerChangePasswordModel
    {
        public int customer_id { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password Old")]
        [StringLength(50, MinimumLength = 6)]
        public string customer_password_old { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password New")]
        [StringLength(50, MinimumLength = 6)]
        public string customer_password { get; set; }
        [Required]
        [Display(Name = "Password New Comfirm")]
        [StringLength(50, MinimumLength = 6)]
        [Compare("customer_password", ErrorMessage = "The password and confirmation password do not match.")]
        public string customer_password_confirm { get; set; }
    }

}