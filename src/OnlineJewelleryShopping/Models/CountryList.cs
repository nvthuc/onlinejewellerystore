﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Models
{
    public partial class CountryList 
    {

        public static SelectList GetListcountry(string country_name)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Selected = true });
            using(var country=new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                var listcoun = country.countries.ToList();
                foreach(var item in listcoun)
                {
                    var add = new SelectListItem()
                    {
                        Value = item.country_name.ToString(),
                        Text = item.country_name.ToString()
                    };
                    list.Add(add);
                }

            }

            var re = new SelectList(list, "Value", "Text");
            return re;

        }
    }

}
