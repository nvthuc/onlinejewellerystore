//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnlineJewelleryShopping.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class product_discount_detail
    {
        public Nullable<int> product_discount_id { get; set; }
        public Nullable<int> product_id { get; set; }
        public Nullable<int> category_id { get; set; }
        public int id { get; set; }
    
        public virtual category category { get; set; }
        public virtual product product { get; set; }
        public virtual product_discount product_discount { get; set; }
    }
}
