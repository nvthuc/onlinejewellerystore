﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Models
{
    public class ModelRegister
    {
        public int customer_id { get; set; }
        [Required]
        [Display(Name = "First Name")]
        [MaxLength(50)]
        public string customer_firstname { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        [MaxLength(50)]
        public string customer_lastname { get; set; }
        public string customer_company { get; set; }
        [Required]
        [Display(Name = "Address1 Name")]
        [MaxLength(100)]
        public string customer_address1 { get; set; }
        public string customer_address2 { get; set; }
        [Required]
        [Display(Name = "City")]
        [MaxLength(50)]
        public string customer_city { get; set; }
        [Required]
        [Display(Name = "Zipcode")]
        [MaxLength(50)]
        public string customer_zipcode { get; set; }
        [Required]
        [Display(Name = "State")]
        [MaxLength(50)]
        public string customer_state { get; set; }
        public string customer_country { get; set; }
        [Required]
        [Display(Name = "Phone")]
        [MaxLength(50)]
        public string customer_phone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [MaxLength(50)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}",
          ErrorMessage = "Please enter correct email")]
        public string customer_email { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 6)]
        [Display(Name = "Password")]
        public string customer_password { get; set; }
        public Nullable<bool> customer_block { get; set; }
        public Nullable<System.DateTime> customer_date { get; set; }

    }
}