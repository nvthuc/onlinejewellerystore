﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class EmailAddress : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            //Cast value to string
            var obj = value as string;
            //Check email by using  System.Net.Mail.MailAddress
            try
            {
                //Mail is valid
                System.Net.Mail.MailAddress mail = new System.Net.Mail.MailAddress(obj);
                return true;
            }
            catch (Exception)
            {
                //Mail invalid
                return false;
            }
        }
    }
    public class AdminEmailExist : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            //Cast value to string
            var obj = value as string;
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                if (ctx.admin_login.Where(m => m.admin_username == obj).ToList().Count() > 0)
                {
                    //Email is areadly exist
                    this.ErrorMessage = "Email is already exist.";
                    return false;
                }
                else
                {
                    return true;
                }
            }

        }

    }
}