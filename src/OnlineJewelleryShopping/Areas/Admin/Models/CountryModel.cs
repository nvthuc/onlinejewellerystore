﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class CountryModel
    {
        public static SelectList GetCountry()
        {
            //declare list
            var lst = new List<SelectListItem>();
            //get data from database
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //all country from database
                var lsCountry = ctx.countries.ToList();
                //add to lst
                foreach (var item in lsCountry)
                {
                    lst.Add(new SelectListItem()
                    {
                        Text = item.country_name,
                        Value = item.country_name
                    });
                }
            }
            //return result
            return new SelectList(lst, "Value", "Text");
        }
    }
}