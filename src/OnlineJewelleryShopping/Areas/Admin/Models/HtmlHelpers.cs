using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.Mvc.Html;

public static partial class HtmlHelpers
{
    public static MvcHtmlString ClassParent(this HtmlHelper htmlHelper, string controller)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
        if (currentController.ToUpper().Equals(controller.ToUpper()))
        {
            return new MvcHtmlString("class=\"current\"");
        }
        else
        {
            return new MvcHtmlString("class=\"select\"");
        }


    }
    public static MvcHtmlString ClassParentMore(this HtmlHelper htmlHelper)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller").ToUpper();
        System.Collections.Generic.List<string> array = new System.Collections.Generic.List<string> { "ACOUNTANT", "ABOUTUS", "CONTACTSINFO", "TERMANDCONDITIONAL" };
        if (array.Contains(currentController))
        {
            return new MvcHtmlString("class=\"current\"");
        }
        else
        {
            return new MvcHtmlString("class=\"select\"");
        }


    }
    public static MvcHtmlString ClassChild(this HtmlHelper htmlHelper, string controller)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");


        if (currentController.ToUpper().Equals(currentController.ToUpper()))
        {
            return new MvcHtmlString("class=\"select_sub show\"");
        }
        else
        {
            return new MvcHtmlString("class=\"select_sub\"");

        }


    }
    public static MvcHtmlString ClassChildMore(this HtmlHelper htmlHelper)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
        System.Collections.Generic.List<string> array = new System.Collections.Generic.List<string> { "ACOUNTANT", "ABOUTUS", "CONTACTSINFO", "TERMANDCONDITIONAL" };

        if (array.Contains(currentController.ToUpper()))
        {
            return new MvcHtmlString("class=\"select_sub show\"");
        }
        else
        {
            return new MvcHtmlString("class=\"select_sub\"");

        }


    }
    public static MvcHtmlString ClassChildCurrent(this HtmlHelper htmlHelper, string actionName, string controllerName)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
        var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
        if (currentController.ToUpper() == controllerName.ToUpper() && currentAction.ToUpper() == actionName.ToUpper())
        {
            return new MvcHtmlString("class=\"sub_show\"");
        }
        else
        {
            return new MvcHtmlString("");
        }

    }
    public static MvcHtmlString ClassChildCurrentMore(this HtmlHelper htmlHelper, string controllerName)
    {
        var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");
        if (currentController.ToUpper().Equals(controllerName.ToUpper()))
        {
            return new MvcHtmlString("class=\"sub_show\"");
        }
        else
        {
            return new MvcHtmlString("");
        }

    }
    
}