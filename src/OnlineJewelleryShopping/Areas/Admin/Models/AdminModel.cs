﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class AdminCreateModel
    {
        [Required(ErrorMessage = "Email is requied.")]
        [EmailAddress(ErrorMessage = "Email is not valid format.")]
        [AdminEmailExist(ErrorMessage = "Email is aready exist.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is requied.")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password confirm is requied.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Full Name is requied.")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Phone is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string Phone { get; set; }
    }
    public class AdminEditModel
    {
        [Required]
        public int ID { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Email is not valid format.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Full Name is required.")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Phone is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string Phone { get; set; }
    }
    public class AdminResetPassModel
    {
        [Required]
        public int ID { get; set; }

        [Required(ErrorMessage = "New password is required")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Password confirm is required" )]
        public string ConfirmPassword { get; set; }
    }
   
}