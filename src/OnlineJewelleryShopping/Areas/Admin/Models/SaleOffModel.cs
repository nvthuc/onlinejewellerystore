﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class SaleOffModel
    {
        public int product_discount_id { get; set; }
        [Required(ErrorMessage = "Start date is required.")]
        public Nullable<System.DateTime> product_discount_startdate { get; set; }
        [Required(ErrorMessage = "End date is required.")]
        public Nullable<System.DateTime> product_discount_enddate { get; set; }
        [Required(ErrorMessage = "Amount date is required.")]
        [Range(0,100,ErrorMessage = "Amount is out of range from 0 to 100 percent.")]
        public Nullable<double> product_discount_amount { get; set; }
        [Required(ErrorMessage = "Name date is required.")]
        public string product_discount_name { get; set; }
        public static SelectList GetSelectList(SelectListOption option) {
            var lstObj = new List<SelectListItem>();
            if (option == SelectListOption.HaveEmpty)
            {
                lstObj.Add(new SelectListItem());
            }
            else
            {
                lstObj.Add(new SelectListItem() { Value = "", Text = "All" });
            }
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (var item in ctx.product_discount.Where(m => m.product_discount_startdate <= DateTime.Today && m.product_discount_enddate >= DateTime.Today))
                {
                    lstObj.Add(new SelectListItem() { Value = item.product_discount_id.ToString(), Text = item.product_discount_name });
                }
            }
            return new SelectList(lstObj, "Value","Text");
        
        }
    }

    public enum SelectListOption{
        HaveEmpty,
        HaveAll
}
    
}