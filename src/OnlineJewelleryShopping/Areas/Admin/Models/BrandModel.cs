﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    //Model class for create brand

    //Model class for Edit brand
    public class BrandModel
    {
        public int brand_id { get; set; }
        [Required(ErrorMessage = "Brand name is requied")]
        [Display(Name = "Brand name :")]
        [StringLength(100, ErrorMessage = "Max lenght of brand is 100.")]
        public string brand_name { get; set; }
        [Required(ErrorMessage = "Brand address is requied")]
        [Display(Name = "Brand Address :")]
        public string brand_address { get; set; }
        [Required(ErrorMessage = "Brand phone number is requied")]
        [RegularExpression("^\\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        [Display(Name = "Brand Phone number :")]
        public string brand_phone { get; set; }
        [Display(Name = "Brand Fax :")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Fax number in valid fomat.")]
        public string brand_fax { get; set; }
        [Display(Name = "Brand description :")]
        [AllowHtml]
        public string brand_descripttion { get; set; }
        public static SelectList GetListBrands()
        {

            //declare list 
            var lst = new List<SelectListItem>();
            //Add empty to first element
           
            //get list brand from database 
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //get list
                var lstBrand = ctx.brands.ToList();
                //Add to list 
                foreach (var item in lstBrand)
                {
                    lst.Add(new SelectListItem()
                    {
                        Value = item.brand_id.ToString(),
                        Text = item.brand_name
                    });
                }
            }
            return new SelectList(lst, "Value", "Text");
        }
        public static SelectList GetFilter()
        {

            //declare list 
            var lst = new List<SelectListItem>();
            //Add empty to first element
            lst.Add(new SelectListItem() { Text = "All", Value = "0" });
            //get list brand from database 
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //get list
                var lstBrand = ctx.brands.ToList();
                //Add to list 
                foreach (var item in lstBrand)
                {
                    lst.Add(new SelectListItem()
                    {
                        Value = item.brand_id.ToString(),
                        Text = item.brand_name
                    });
                }
            }
            return new SelectList(lst, "Value", "Text");
        }
    }
}