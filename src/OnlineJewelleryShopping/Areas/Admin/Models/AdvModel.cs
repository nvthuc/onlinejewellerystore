﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class AdvModel
    {
        public int advertise_id { get; set; }
        [Required(ErrorMessage = "Position is required")]
        
        public string advertise_name { get; set; }
       
        [Required(ErrorMessage = "Image is requied" )]
        public string advertise_image { get; set; }
        
        [Required(ErrorMessage = "Url is required")]
        public string advertise_url { get; set; }
        public string advertise_target { get; set; }
        
        public DateTime advertise_date { get; set; }
    }
}