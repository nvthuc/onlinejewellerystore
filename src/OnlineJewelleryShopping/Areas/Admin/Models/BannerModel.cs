﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class BannerModel
    {
        public int banner_id { get; set; }
        [Required(ErrorMessage = "Image is requied.")]
        public string banner_image { get; set; }
        [Required(ErrorMessage ="URL is requied.")]
        public string banner_url { get; set; }

    }
}