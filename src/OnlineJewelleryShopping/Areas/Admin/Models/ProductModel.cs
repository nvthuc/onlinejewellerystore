﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    //Model for propduct
    //Product View Models
    //Model for Create Product action

    public class ProductModel
    {
        public int product_id { get; set; }
        [Required(ErrorMessage = "Category is requied.")]
        public int category_id { get; set; }
        [Required(ErrorMessage = "Brand is requied.")]
        public int brand_id { get; set; }
        [Required(ErrorMessage = "Name is requied.")]
        public string product_name { get; set; }
        [Required(ErrorMessage = "Price is requied.")]
        [Range(0, double.MaxValue, ErrorMessage = "Enter price in double value.")]
        public double product_price { get; set; }
        [Required(ErrorMessage = "Quantity is requied.")]
        [RegularExpression("^\\d+$")]
        public int product_quantity { get; set; }

        public string product_material { get; set; }

        public string product_weight { get; set; }

        public string product_dimention { get; set; }

        public bool? product_status { get; set; }
        [AllowHtml]
        public string product_descripttion { get; set; }
        public string product_color { get; set; }
        [Required(ErrorMessage = "Image is requied.")]

        public string product_images_01 { get; set; }
        [Required(ErrorMessage = "Image is requied.")]

        public string product_images_02 { get; set; }
        [Required(ErrorMessage = "Image is requied.")]

        public string product_images_03 { get; set; }
        [Required(ErrorMessage = "Image is requied.")]
        public string product_images_04 { get; set; }
        [Required(ErrorMessage = "Image is requied.")]
        public string product_images_05 { get; set; }
        public Nullable<System.DateTime> product_dateadd { get; set; }

    }
}