﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class CustommerModel
    {
        public int customer_id { get; set; }
        [Required(ErrorMessage = "Firstname is requied.")]
        public string customer_firstname { get; set; }
        [Required(ErrorMessage = "Lastname is requied.")]
        public string customer_lastname { get; set; }
       
        public string customer_company { get; set; }
        [Required(ErrorMessage = "Address is requied.")]
        public string customer_address1 { get; set; }
     
        public string customer_address2 { get; set; }
        [Required]
        public string customer_city { get; set; }
        [Required(ErrorMessage = "Zipcode is requied.")]
        [RegularExpression("^\\d+$", ErrorMessage = "Zip coed is number.")]
        public string customer_zipcode { get; set; }
        [Required(ErrorMessage = "State is requied.")]
        public string customer_state { get; set; }
        [Required(ErrorMessage = "Country is requied.")]
        public string customer_country { get; set; }
        [Required(ErrorMessage = "Phone is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string customer_phone { get; set; }
        [Required(ErrorMessage = "Email is requied.")]
        [EmailAddress(ErrorMessage = "Email is invaild format")]
        public string customer_email { get; set; }
        [Required(ErrorMessage = "Password is requied.")]
        public string customer_password { get; set; }
        [Required(ErrorMessage = "Password confirm is requied.")]
        public string customer_password_confirm { get; set; }
        public Nullable<bool> customer_block { get; set; }
        public Nullable<System.DateTime> customer_date { get; set; }
    }
    public class CustommerEditModel
    {
        public int customer_id { get; set; }
        [Required(ErrorMessage = "Firstname is requied.")]
        public string customer_firstname { get; set; }
        [Required(ErrorMessage = "Lastname is requied.")]
        public string customer_lastname { get; set; }

        public string customer_company { get; set; }
        [Required(ErrorMessage = "Address is requied.")]
        public string customer_address1 { get; set; }

        public string customer_address2 { get; set; }
        [Required(ErrorMessage = "City is requied.")]
        public string customer_city { get; set; }
        [Required(ErrorMessage = "Zipcode is requied.")]
        [RegularExpression("^\\d+$", ErrorMessage = "Zip coed is number.")]
        public string customer_zipcode { get; set; }
        [Required(ErrorMessage = "State is requied.")]
        public string customer_state { get; set; }
        [Required(ErrorMessage = "Country is requied.")]
        public string customer_country { get; set; }
        [Required(ErrorMessage = "Phone is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string customer_phone { get; set; }
        [Required(ErrorMessage = "Email is requied.")]
        [EmailAddress(ErrorMessage = "Email is invaild format")]
        public string customer_email { get; set; }      
        public Nullable<bool> customer_block { get; set; }
        public Nullable<System.DateTime> customer_date { get; set; }
    }
    public class CustomerResetPassword
    {
        public int id { get; set; }
        [Required(ErrorMessage = "New password is required.")]
        [MinLength(6,ErrorMessage = "Password is at least 6 charecter")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Confirm password is required.")]
        public string ConfirmPassword { get; set; }
        public string ReturnUrl { get; set; }
    }
}