﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class OrderModel
    {
        public int orders_id { get; set; }
        public Nullable<int> customer_id { get; set; }
        [Required(ErrorMessage = "Firstname is requied.")]
        public string billing_firstname { get; set; }
        [Required(ErrorMessage = "Lastname is requied.")]
        public string billing_lastname { get; set; }
        [Required(ErrorMessage = "Address is requied.")]
        public string billing_address1 { get; set; }
        public string billing_address2 { get; set; }
        public string billing_city { get; set; }
        [Required(ErrorMessage = "Zipcode is requied.")]
        public string billing_zipcode { get; set; }
        [Required(ErrorMessage = "State is requied.")]
        public string billing_state { get; set; }
        public string billing_country { get; set; }
        [Required(ErrorMessage = "Phone is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string billing_phone { get; set; }
       
        public string shipping_firstname { get; set; }
        
        public string shipping_lastname { get; set; }
        
        public string shipping_address1 { get; set; }
        public string shipping_address2 { get; set; }
        public string shipping_city { get; set; }
        public string shipping_company { get; set; }
        public string billing_company { get; set; }
        public string shipping_zipcode { get; set; }
        public string shipping_state { get; set; }
        public string shipping_country { get; set; }
        public string shipping_phone { get; set; }
        public string orders_status { get; set; }
        public  float orders_vat { get; set; }
        public string orders_payment { get; set; }
        public string orders_comment { get; set; }
        public float orders_total { get; set; }
        public Nullable<System.DateTime> orders_date { get; set; }

    }
    public class OrderListProduct
    {
        public int id { get; set; }
        public string image { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
        public double discount { get; set; }
    }
}