﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Address is requied.")]
        public string address { get; set; }
        [Required(ErrorMessage = "Email is requied.")]
        [EmailAddress(ErrorMessage = "Email not valid.")]
        public string email { get; set; }

        public string fax { get; set; }
        [Required(ErrorMessage = "Phone number is requied.")]
        public string phone { get; set; }
        public string skype { get; set; }
    }
}