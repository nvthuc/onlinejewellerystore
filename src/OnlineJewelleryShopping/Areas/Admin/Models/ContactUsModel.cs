﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class ContactUsModel
    {
        public int contact_us_id { get; set; }
        [Required(ErrorMessage = "Fullname is requied.")]
        public string contact_us_fullname { get; set; }
        [Required(ErrorMessage = "Email is requied.")]
        [EmailAddress(ErrorMessage = "Email is invalid format")]
        public string contact_us_email { get; set; }
        [Required(ErrorMessage = "Phone number is requied.")]
        [RegularExpression(@"^\+?[0-9]{3}-?[0-9]{6,12}$", ErrorMessage = "Phone number in valid fomat.")]
        public string contact_us_phone { get; set; }
        [Required(ErrorMessage = "Enquiry is requied.")]
        public string contact_us_enquiry { get; set; }
       
    }
}