﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class AcountantModel
    {
        [Required(ErrorMessage = "VAT is reqiued")]
        [Range(0,100,ErrorMessage = "Please enter VAT percent (double value).")]
        public float account_tant_vat { get; set; }
        //[Required]
        //[RegularExpression("/d+")]
        //public double account_tant_discount { get; set; }
    }
   
}