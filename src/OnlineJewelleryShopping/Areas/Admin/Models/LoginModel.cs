﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter Email.")]
        [EmailAddress(ErrorMessage = "Email is not valid format.")]
       
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter your password")]
        public string Password { get; set; }
        public bool IsRemember { get; set; }
    }

    
}