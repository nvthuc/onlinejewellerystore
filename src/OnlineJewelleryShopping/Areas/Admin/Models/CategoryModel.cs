﻿using OnlineJewelleryShopping.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Areas.Admin.Models
{
    //model for category
    public class CategoryModel
    {
        public CategoryModel()
        {
            
        }
        public int category_id { get; set; }
        [Required(ErrorMessage = "Category name is riquired.")]
        public string category_name { get; set; }
        [AllowHtml]
        public string category_descripttion { get; set; }
        //[Required(ErrorMessage  = "Image is required")]
        public string category_image { get; set; }
        public Nullable<int> category_parent { get; set; }

        //Delete 
        public bool IsDelete { get; set; }
        //return datasource for dropdownlist category
        public static SelectList Get2Level(){
            //init selectlistitem collection 
            var lst = new List<SelectListItem>();
            ////add empty select
            //lst.Add(new SelectListItem() { Selected = true, Text = "", Value = "" });
            //get category from database
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //get top level category list
                foreach (var item in ctx.categories.Where(m => m.category_parent == null))
                {
                    //init top category
                    var slt = new SelectListItem() {
                        Value = item.category_id.ToString(), 
                        Text = item.category_name
                    };
                    //add to list
                    lst.Add(slt);
                    //get second level
                    foreach (var it in ctx.categories.Where(m => m.category_parent == item.category_id))
                    {
                        //init second level
                        var slt2 = new SelectListItem()
                        {
                            Value = it.category_id.ToString(),
                            Text = "--" + it.category_name
                        };
                       //Add to list
                        lst.Add(slt2);
                    }
                }
            }
            //init select list
            var re = new SelectList(lst,"Value", "Text");
            //return
            return re;
        }
        public static SelectList GetTopLevel()
        {
            //init selectlistitem collection 
            var lst = new List<SelectListItem>();
            //add empty select
            lst.Add(new SelectListItem() { Selected = true });
            //get category from database
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //get top level category list
                foreach (var item in ctx.categories.Where(m => m.category_parent == null))
                {
                    //init top category
                    var slt = new SelectListItem()
                    {
                        Value = item.category_id.ToString(),
                        Text = item.category_name
                    };
                    //add to list
                    lst.Add(slt);
                }
            }
            return new SelectList(lst, "Value", "Text");
        }
        public static SelectList GetFilter()
        {
            //init selectlistitem collection 
            var lst = new List<SelectListItem>();
            //add empty select
            lst.Add(new SelectListItem() { Selected = true , Text = "All", Value = "0"});
            //get category from database
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                //get top level category list
                foreach (var item in ctx.categories.Where(m => m.category_parent == null))
                {
                    //init top category
                    var slt = new SelectListItem()
                    {
                        Value = item.category_id.ToString(),
                        Text = item.category_name
                    };
                    //add to list
                    lst.Add(slt);

                    //Find child category
                    //get second level
                    foreach (var it in ctx.categories.Where(m => m.category_parent == item.category_id))
                    {
                        //init second level
                        var slt2 = new SelectListItem()
                        {
                            Value = it.category_id.ToString(),
                            Text = "--" + it.category_name
                        };
                        //Add to list
                        lst.Add(slt2);
                    }
                }
            }
            return new SelectList(lst, "Value", "Text");
        }
    }
}