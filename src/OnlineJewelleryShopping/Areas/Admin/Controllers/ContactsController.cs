﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class ContactsController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Contacts/
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.contact_us.Remove(ctx.contact_us.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Index(string Keyword = "", string Filter = "All", string Order = "desc", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Curent Filter
            ViewBag.Filter = Filter;

            //Current Order
            ViewBag.Order = Order;

            //All contact
            var lstObj = from c in db.contact_us select c;

            //Filter module
            switch (Filter)
            {
                case "Name":
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.contact_us_fullname.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }


                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.contact_us_fullname);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.contact_us_fullname);
                    }
                    break;
                case "Enquiry":
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.contact_us_enquiry.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.contact_us_enquiry);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.contact_us_enquiry);
                    }
                    break;

                default:
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.contact_us_fullname.ToUpper().Contains(Keyword.ToUpper()) || c.contact_us_enquiry.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.contact_us_id);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.contact_us_id);
                    }
                    break;
            }


            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }

        //
        // GET: /Admin/Contacts/Details/5

        public ActionResult Details(int id = 0)
        {
            //Find in database 
            contact_us contact_us = db.contact_us.Find(id);
            //Check result
            if (contact_us == null) //Not found
            {
                return HttpNotFound();
            }
            //Found
            //Render View
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Create

        public ActionResult Create()
        {
            //Render create form
            return View();
        }

        //
        // POST: /Admin/Contacts/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactUsModel contact_us)
        {
            //Check ModelState
            if (ModelState.IsValid) //Model is valid
            {
                //Add new contact 
                db.contact_us.Add(new contact_us()
                {
                    contact_us_phone = contact_us.contact_us_phone,
                    contact_us_fullname = contact_us.contact_us_fullname,
                    contact_us_enquiry = contact_us.contact_us_enquiry,
                    contact_us_email = contact_us.contact_us_email,
                    contact_us_date = DateTime.Now
                });
                //Save
                db.SaveChanges();
                //Redirect to index action
                return RedirectToAction("Index");
            }
            //ModelState is invalid
            //Return view and display error message
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in database 
            contact_us contact_us = db.contact_us.Find(id);
            //Check result
            if (contact_us == null) //Not found: error 404
            {
                return HttpNotFound();
            }
            //Found 
            //Render form  edit with strong-type
            return View(new ContactUsModel()
            {
                contact_us_email = contact_us.contact_us_email,
                contact_us_enquiry = contact_us.contact_us_enquiry,
                contact_us_fullname = contact_us.contact_us_fullname,
                contact_us_phone = contact_us.contact_us_phone,
                contact_us_id = contact_us.contact_us_id
            });
        }

        //
        // POST: /Admin/Contacts/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactUsModel contact_us)
        {
            //Check modelstate
            if (ModelState.IsValid) //Valid
            {
                //Get from database
                var obj = db.contact_us.Find(contact_us.contact_us_id);
                //Assign new value
                obj.contact_us_email = contact_us.contact_us_email;
                obj.contact_us_enquiry = contact_us.contact_us_enquiry;
                obj.contact_us_fullname = contact_us.contact_us_fullname;
                obj.contact_us_phone = contact_us.contact_us_phone;
                //Mark obj as modified
                db.Entry(obj).State = EntityState.Modified;
                //Save
                db.SaveChanges();
                //Redirect to index action
                return RedirectToAction("Index");
            }
            //Invalid
            //return view and display error message
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //Find in database
            contact_us contact_us = db.contact_us.Find(id);
            //Check result
            if (contact_us == null) //Not found error 404
            {
                return HttpNotFound();
            }
            //Found
            //Render confirm View 
            return View(contact_us);
        }

        //
        // POST: /Admin/Contacts/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Find in database
            contact_us contact_us = db.contact_us.Find(id);
            //Remove
            db.contact_us.Remove(contact_us);
            //Save
            db.SaveChanges();
            //Redirect to index action
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}