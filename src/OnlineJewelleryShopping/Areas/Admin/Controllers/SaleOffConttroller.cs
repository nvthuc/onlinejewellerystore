﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class SaleOffController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Contacts/
       
        public ActionResult Index(string Keyword = "", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;

            //All Sale off
            var lstObj = from c in db.product_discount select c;

            //Search modile
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj where i.product_discount_name.ToUpper().Contains(Keyword.ToUpper()) == true select i;
            }
            lstObj = from i in lstObj orderby i.product_discount_id descending select i;
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }

        //
        // GET: /Admin/Contacts/Details/5

        public ActionResult Details(int id = 0)
        {
            //Find in database 
            var contact_us = db.product_discount.Find(id);
            //Check result
            if (contact_us == null) //Not found
            {
                return HttpNotFound();
            }
            //Found

            //Count Product
            ViewBag.CountProduct = db.product_discount_detail.Where(p => p.product_discount_id == id).Count();
            //Render View
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Create

        public ActionResult Create()
        {
            //Render create form
            return View();
        }

        //
        // POST: /Admin/Contacts/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SaleOffModel contact_us)
        {
            //Check ModelState
            if (ModelState.IsValid) //Model is valid
            {
                //Add new contact 
                db.product_discount.Add(new product_discount()
                {
                    product_discount_name = contact_us.product_discount_name,
                    product_discount_enddate = contact_us.product_discount_enddate,
                    product_discount_startdate = contact_us.product_discount_startdate,
                    product_discount_amount = contact_us.product_discount_amount

                });
                //Save
                db.SaveChanges();
                //Redirect to index action
                return RedirectToAction("Index");
            }
            //ModelState is invalid
            //Return view and display error message
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in database 
            var contact_us = db.product_discount.Find(id);
            //Check result
            if (contact_us == null) //Not found: error 404
            {
                return HttpNotFound();
            }
            //Found 
            //Render form  edit with strong-type
            return View(new SaleOffModel()
            {
                product_discount_amount = contact_us.product_discount_amount,
                product_discount_enddate = contact_us.product_discount_enddate,
                product_discount_startdate = contact_us.product_discount_startdate,
                product_discount_name = contact_us.product_discount_name,
                product_discount_id = contact_us.product_discount_id
            });
        }

        //
        // POST: /Admin/Contacts/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SaleOffModel contact_us)
        {
            //Check modelstate
            if (ModelState.IsValid) //Valid
            {
                //Get from database
                var obj = db.product_discount.Find(contact_us.product_discount_id);
                //Assign new value
                obj.product_discount_amount = contact_us.product_discount_amount;
                obj.product_discount_enddate = contact_us.product_discount_enddate;
                obj.product_discount_name = contact_us.product_discount_name;
                obj.product_discount_startdate = contact_us.product_discount_startdate;
                //Mark obj as modified
                db.Entry(obj).State = EntityState.Modified;
                //Save
                db.SaveChanges();
                //Redirect to index action
                return RedirectToAction("Index");
            }
            //Invalid
            //return view and display error message
            return View(contact_us);
        }

        //
        // GET: /Admin/Contacts/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //Find in database
            var contact_us = db.product_discount.Find(id);
            //Check result
            if (contact_us == null) //Not found error 404
            {
                return HttpNotFound();
            }
            //Found

            //Count Product
            ViewBag.CountProduct = db.product_discount_detail.Where(p => p.product_discount_id == id).Count();

            //Render confirm View 
            return View(contact_us);
        }

        //
        // POST: /Admin/Contacts/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Find in database
            var contact_us = db.product_discount.Find(id);
            //Remove
            db.product_discount.Remove(contact_us);
            //Save
            db.SaveChanges();
            //Redirect to index action
            return RedirectToAction("Index");
        }
        //Add product to sale Off=================================================
        #region Add product to sale off
        [HttpPost]
        public ActionResult AddProduct(int id, FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int key = int.Parse(item.ToString());
                    ctx.product_discount_detail.Add(new product_discount_detail() { product_id = key, product_discount_id = id });
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("AddProduct", new { id = id });
        }
        public ActionResult AddProduct(int id, string Keyword = "", int Pagenumber = 1, int PageSize = 10, int Brand = 0, int Category = 0)
        {
            //Find sale off in database 
            var objSaleOff = db.product_discount.Find(id);

            //Check result
            if (objSaleOff == null) //Not found data
            {
                return HttpNotFound();
            }

            //Found saleOff==========================================================================================

            //Current ID
            ViewBag.CurrentID = id.ToString();

            //Sale off Name
            ViewBag.SaleOffName = objSaleOff.product_discount_name;

            //Sale off Percent
            ViewBag.SaleOffPercent = objSaleOff.product_discount_amount + "%";

            //Sale off From
            ViewBag.SaleOffFrom = objSaleOff.product_discount_startdate.Value.ToShortDateString();

            //Sale off To
            ViewBag.SaleOffTo = objSaleOff.product_discount_enddate.Value.ToShortDateString();

            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Brand Dropdownlist
            ViewBag.Brands = BrandModel.GetFilter();
            //Current Brand
            ViewBag.CurrentBrand = Brand.ToString();

            //Category Dropdownlist
            ViewBag.Categories = CategoryModel.GetFilter();
            //Current Category
            ViewBag.CurrentCategory = Category.ToString();

            //List product ID in sale Off
            var lstPID = from p in db.product_discount_detail where p.product_discount_id == id select p.product_id;

            //All product not in sale off
            var lstObj = from c in db.products where !lstPID.Contains(c.product_id) && c.product_status == true select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }
            //Brand filter
            if (Brand != 0)
            {
                lstObj = from i in lstObj where i.brand_id == Brand orderby i.brand.brand_name ascending select i;
            }

            //Category filter
            if (Category != 0)
            {
                lstObj = from i in lstObj
                         where i.category_id == Category ||
                             (from c in db.categories where c.category_parent == Category select c.category_id).Contains((int)i.category_id)
                         orderby i.category.category_name ascending
                         select i;
            }
            if (Brand == 0 && Category == 0)
            {
                lstObj = from i in lstObj orderby i.product_name ascending select i;
            }
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion

        //Remove product to sale Off=================================================
        #region Remove product to sale off

        //Process post remove product from saleoff
        [HttpPost]
        public ActionResult RemoveProduct(int id, FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int key = int.Parse(item.ToString());
                    ctx.Entry(ctx.product_discount_detail.Where(m => m.product_discount_id == id && m.product_id == key).FirstOrDefault()).State = EntityState.Deleted;
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("RemoveProduct", new { id = id });
        }
        public ActionResult RemoveProduct(int id, string Keyword = "", int Pagenumber = 1, int PageSize = 10, int Brand = 0, int Category = 0)
        {
            //Find sale off in database 
            var objSaleOff = db.product_discount.Find(id);

            //Check result
            if (objSaleOff == null) //Not found data
            {
                return HttpNotFound();
            }

            //Found saleOff==========================================================================================

            #region Data to View

            //Current ID
            ViewBag.CurrentID = id;

            //Sale off Name
            ViewBag.SaleOffName = objSaleOff.product_discount_name;

            //Sale off Percent
            ViewBag.SaleOffPercent = objSaleOff.product_discount_amount + "%";

            //Sale off From
            ViewBag.SaleOffFrom = objSaleOff.product_discount_startdate.Value.ToShortDateString();

            //Sale off To
            ViewBag.SaleOffTo = objSaleOff.product_discount_enddate.Value.ToShortDateString();

            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Brand Dropdownlist
            ViewBag.Brands = BrandModel.GetFilter();
            //Current Brand
            ViewBag.CurrentBrand = Brand.ToString();

            //Category Dropdownlist
            ViewBag.Categories = CategoryModel.GetFilter();
            //Current Category
            ViewBag.CurrentCategory = Category.ToString();
            #endregion

            #region Search and Filter
            //List product ID in sale Off
            var lstPID = from p in db.product_discount_detail where p.product_discount_id == id select p.product_id;

            //All product not in sale off
            var lstObj = from c in db.products where lstPID.Contains(c.product_id) && c.product_status == true select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }
            //Brand filter
            if (Brand != 0)
            {
                lstObj = from i in lstObj where i.brand_id == Brand orderby i.brand.brand_name ascending select i;
            }

            //Category filter
            if (Category != 0)
            {
                lstObj = from i in lstObj
                         where i.category_id == Category ||
                             (from c in db.categories where c.category_parent == Category select c.category_id).Contains((int)i.category_id)
                         orderby i.category.category_name ascending
                         select i;
            }
            if (Brand == 0 && Category == 0)
            {
                lstObj = from i in lstObj orderby i.product_name ascending select i;
            }
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
            #endregion
        #endregion

        #region Remove All Product in SaleOff
        public ActionResult RemoveAllProduct(int id) {
            foreach (var item in (from i in db.product_discount_detail where i.product_discount_id == id select i))
            {
                db.Entry(item).State = EntityState.Deleted;
                
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}