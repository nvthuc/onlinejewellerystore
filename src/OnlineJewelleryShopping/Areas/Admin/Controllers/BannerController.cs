﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class BannerController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Banner/

        public ActionResult Index(int PageNumber = 1, int PageSize = 5)
        {
            //Get all banner
            var lstObj = from b in db.banners orderby b.banner_id select b;
            //Render view with model
            return View(lstObj.ToPagedList(PageNumber, PageSize));
        }

        //
        // GET: /Admin/Banner/Details/5
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {

            foreach (string item in collection.Keys)
            {
                int id = int.Parse(item);
                db.banners.Remove(db.banners.Find(id));
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        public ActionResult Details(int id = 0)
        {
            //Find in database
            banner banner = db.banners.Find(id);
            if (banner == null) //Not found return error 404
            {
                return HttpNotFound();
            }
            //Found
            //Render View
            return View(banner);
        }

        //
        // GET: /Admin/Banner/Create

        public ActionResult Create()
        {
            //Render Create form
            return View();
        }

        //
        // POST: /Admin/Banner/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BannerModel banner)
        {
            //Check ModelState
            if (ModelState.IsValid)  //Model is valid
            {
                //Add new banner to database 
                db.banners.Add(new banner()
                {
                    banner_image = banner.banner_image,
                    banner_url = banner.banner_url
                });
                //Save
                db.SaveChanges();
                //Redirect to Index action
                return RedirectToAction("Index");
            }
            //Model is invalid
            //Return view and display error message
            return View(banner);
        }

        //
        // GET: /Admin/Banner/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in  dtaabase
            banner banner = db.banners.Find(id);

            //Check result
            if (banner == null) //Not found in database error 404
            {
                return HttpNotFound();
            }
            //Found
            //Render edit form
            return View(new BannerModel()
            {
                banner_id = banner.banner_id,
                banner_image = banner.banner_image,
                banner_url = banner.banner_url
            });
        }

        //
        // POST: /Admin/Banner/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BannerModel banner)
        {
            //Check Model
            if (ModelState.IsValid) //Model is valid
            {
                var ban = new banner();
                ban.banner_id = banner.banner_id;
                ban.banner_image = banner.banner_image;
                ban.banner_url = banner.banner_url;
                db.Entry(ban).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //Model is invalid
            //Return View and display error message
            return View(banner);
        }

        //
        // GET: /Admin/Banner/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //Find in database
            banner banner = db.banners.Find(id);

            //Check result 
            if (banner == null) //Not found return error 404
            {
                return HttpNotFound();
            }
            //Found 
            //Render view delete confirm
            return View(banner);
        }

        //
        // POST: /Admin/Banner/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Find in database
            banner banner = db.banners.Find(id);
            //Remove
            db.banners.Remove(banner);
            //Save to database
            db.SaveChanges();
            //Redirect to index action
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}