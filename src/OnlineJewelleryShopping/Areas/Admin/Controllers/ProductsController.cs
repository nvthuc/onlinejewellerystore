﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {

        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        #region Index Product
        //
        // GET: /Admin/Products/

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.products.Remove(ctx.products.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Index(string Keyword = "", int Pagenumber = 1, int PageSize = 10, string Brand = "0", string Category = "0", string Quantity = "Available", string Status = "Selling")
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Current Brand
            ViewBag.CurrentBrand = Brand;

            //Current Category
            ViewBag.CurrentCategory = Category;

            //Current Quantity
            ViewBag.CurrentQuantity = Quantity;

            //Current Status
            ViewBag.CurrentStatus = Status;

            //Brand
            ViewBag.Brands = BrandModel.GetFilter();

            //Category
            ViewBag.Categories = CategoryModel.GetFilter();

            //All product
            var lstObj = from c in db.products select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }
            //Brand filter
            if (Brand != null && Brand != "0")
            {
                int id = int.Parse(Brand);
                lstObj = from i in lstObj where i.brand_id == id select i;
            }

            //Category filter
            if (Category != null && Category != "0")
            {
                int id = int.Parse(Category);
                lstObj = from i in lstObj where i.category_id == id || (from c in db.categories where c.category_parent == id select c.category_id).Contains((int)i.category_id) select i;
            }
            switch (Quantity)
            {
                case "All":
                    break;
                case "Available":
                    lstObj = from i in lstObj where i.product_quantity > 0 select i;
                    break;
                case "SoldOut":
                    lstObj = from i in lstObj where i.product_quantity == 0 select i;
                    break;

                default:
                    break;
            }

            //Status
            switch (Status)
            {
                case "All":
                    break;
                case "Selling":
                    lstObj = from i in lstObj where i.product_status == true select i;
                    break;
                case "NotSell":
                    lstObj = from i in lstObj where i.product_status == false select i;
                    break;

                default:
                    break;
            }
            lstObj = from i in lstObj orderby i.product_id descending select i;
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion

        #region Review Product
        //
        // GET: /Admin/Products/

        [HttpPost]
        public ActionResult Review(int id, FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {

                ctx.product_review.Remove(ctx.product_review.Find(id));

                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Review(int id, string Keyword = "", int Pagenumber = 1, int PageSize = 50)
        {
            //Current Key word
            ViewBag.KeyWord = Keyword;

            ViewBag.ID = id.ToString();
            //All review
            var lstObj = from c in db.product_review where c.product_id == id select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_review_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }

            lstObj = from i in lstObj orderby i.product_id descending select i;
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion
       //Hide review
        //Delete

        public ActionResult CreateReview(int id, FormCollection c)
        {
            db.product_review.Add(new product_review()
            {
                product_id = id,
                product_review_name = c["txtName"],
                product_review_text = c["txtContent"]
            });
            return RedirectToAction("Review");
        }
        //
        // GET: /Admin/Products/Details/5

        public ActionResult Details(int id = 0)
        {
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // GET: /Admin/Products/Create

        public ActionResult Create()
        {
            //data for dropdownlist
            ViewBag.Brand = BrandModel.GetListBrands();
            ViewBag.Category = CategoryModel.Get2Level();
            return View();
        }

        //
        // POST: /Admin/Products/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductModel p)
        {
            //check model state
            if (ModelState.IsValid)
            { //valid
                //add product to tabble
                db.products.Add(new product()
                {
                    product_images_01 = p.product_images_01,
                    product_images_02 = p.product_images_02,
                    product_images_03 = p.product_images_03,
                    product_images_04 = p.product_images_04,
                    product_images_05 = p.product_images_05,
                    product_material = p.product_material,
                    product_name = p.product_name,
                    product_price = Convert.ToDouble(p.product_price),
                    product_quantity = p.product_quantity,
                    product_status = true,
                    product_weight = p.product_weight,
                    product_dimention = p.product_dimention,
                    product_descripttion = p.product_descripttion,
                    product_dateadd = DateTime.Now,
                    product_color = p.product_color,
                    category_id = p.category_id,
                    brand_id = p.brand_id
                });
                //Save ro database
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //invalid
            //data for dropdownlist
            ViewBag.Brand = BrandModel.GetListBrands();
            ViewBag.Category = CategoryModel.Get2Level();
            return View(p);
        }

        //
        // GET: /Admin/Products/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in database
            product p = db.products.Find(id);
            //check result
            if (p == null)
            { // Not found
                return HttpNotFound();
            }
            //Found
            ViewBag.Brand = BrandModel.GetListBrands();
            ViewBag.Category = CategoryModel.Get2Level();
            return View(new ProductModel()
            {
                product_id = id,
                product_images_01 = p.product_images_01,
                product_images_02 = p.product_images_02,
                product_images_03 = p.product_images_03,
                product_images_04 = p.product_images_04,
                product_images_05 = p.product_images_05,
                product_material = p.product_material,
                product_name = p.product_name,
                product_price = Convert.ToDouble(p.product_price),
                product_quantity = int.Parse(p.product_quantity.ToString()),
                product_status = p.product_status,
                product_weight = p.product_weight,
                product_dimention = p.product_dimention,
                product_descripttion = p.product_descripttion,
                product_color = p.product_color,
                category_id = int.Parse(p.category_id.ToString()),
                brand_id = int.Parse(p.brand_id.ToString())
            });
        }

        //
        // POST: /Admin/Products/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductModel p)
        {
            if (ModelState.IsValid)
            {
                //get from database
                var pp = db.products.Find(p.product_id);
                //new value
                pp.product_images_01 = p.product_images_01;
                pp.product_images_02 = p.product_images_02;
                pp.product_images_03 = p.product_images_03;
                pp.product_images_04 = p.product_images_04;
                pp.product_images_05 = p.product_images_05;
                pp.product_material = p.product_material;
                pp.product_name = p.product_name;
                pp.product_price = p.product_price;
                pp.product_quantity = p.product_quantity;
                pp.product_status = p.product_status;
                pp.product_weight = p.product_weight;
                pp.product_dimention = p.product_dimention;
                pp.product_descripttion = p.product_descripttion;
                pp.product_color = p.product_color;
                pp.category_id = p.category_id;
                pp.brand_id = p.brand_id;
                //save to database
                db.Entry(pp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Brand = BrandModel.GetListBrands();
            ViewBag.Category = CategoryModel.Get2Level();
            var st = new List<SelectListItem>();
            st.Add(new SelectListItem() { Value = "true", Text = "Selling", Selected = true });
            st.Add(new SelectListItem() { Value = "false", Text = "Not Selling" });
            ViewBag.ST = new SelectList(st, "Value", "Text");
            return View(p);
        }

        //
        // GET: /Admin/Products/Delete/5

        public ActionResult Delete(int id = 0)
        {
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Admin/Products/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            product product = db.products.Find(id);
            if (product.orders_detail.Count() > 0)
            {
                return HttpNotFound();
            }
            db.products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}