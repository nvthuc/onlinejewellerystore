﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class AcountantController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/TermAndConditional/

        public ActionResult Index()
        {
            var obj = db.account_tant.FirstOrDefault();
            if (obj != null)
            {
                return View(new AcountantModel() {  
                account_tant_vat = (float)obj.account_tant_vat//,
                 //account_tant_discount =(double) obj.account_tant_discount
                });

            }
            obj = new account_tant (){account_tant_vat = 10};
            db.account_tant.Add(obj);
            db.SaveChanges();
            return View(new AcountantModel()
            {
                account_tant_vat = (float)obj.account_tant_vat //,
                //account_tant_discount = (double)obj.account_tant_discount
            });
        }
        //
        // GET: /Admin/TermAndConditional/Edit/5
        
        public ActionResult Edit()
        {
            account_tant acc = db.account_tant.FirstOrDefault();
            if (acc == null)
            {
                acc = new account_tant() {   account_tant_vat = 10};
                db.account_tant.Add(acc);
                db.SaveChanges();
                return View(acc);
            }
            return View(new AcountantModel()
            {
                account_tant_vat = (float)acc.account_tant_vat //,
               // account_tant_discount = (double)acc.account_tant_discount
            });
        }

        //
        // POST: /Admin/TermAndConditional/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AcountantModel acc)
        {
            var obj = db.account_tant.FirstOrDefault();
            if (obj != null)
            {
                obj.account_tant_vat = (float)acc.account_tant_vat;
              //  obj.account_tant_discount = acc.account_tant_discount;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            obj = new account_tant() {
                account_tant_vat = (float)acc.account_tant_vat 
                //, account_tant_discount = acc.account_tant_discount
            
            };
            db.account_tant.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}