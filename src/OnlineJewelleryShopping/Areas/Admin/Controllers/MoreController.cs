﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class MoreController : Controller
    {
        //
        // GET: /Admin/More/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "AboutUs");
        }

    }
}
