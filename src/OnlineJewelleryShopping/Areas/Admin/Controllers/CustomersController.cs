﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class CustomersController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Customers/

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    //int id = int.Parse(item);
                    //ctx.customers.Remove(ctx.customers.Find(id));

                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Index(string Keyword = "", string Filter = "All", string Order = "desc", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;

       
            //Curent Filter
            ViewBag.Filter = Filter;

            //Current Order
            ViewBag.Order = Order;

            //All contact
            var lstObj = from c in db.customers select c;

            //Filter module
            switch (Filter)
            {
                case "Name": //Filter by name
                    //Search
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.customer_firstname.ToUpper().Contains(Keyword.ToUpper()) || c.customer_lastname.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }

                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_firstname).ThenByDescending(m => m.customer_lastname);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.customer_firstname).ThenBy(m => m.customer_lastname);
                    }
                    break;
                case "Email": //Filter by Email
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.customer_email.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_email);
                    }
                    else
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_email);
                    }
                    break;
                case "Address": //Filter by Address
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj
                                 where c.customer_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                                        c.customer_address2.ToUpper().Contains(Keyword.ToUpper()) ||
                                        c.customer_city.ToUpper().Contains(Keyword.ToUpper()) ||
                                        c.customer_country.ToUpper().Contains(Keyword.ToUpper()) ||
                                         c.customer_state.ToUpper().Contains(Keyword.ToUpper())
                                 select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_address1);
                    }
                    else
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_address1);
                    }
                    break;
                default:
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj
                                 where
                                     c.customer_firstname.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_lastname.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_address2.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_city.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_country.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_state.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_zipcode.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_email.ToUpper().Contains(Keyword.ToUpper()) ||
                                     c.customer_company.ToUpper().Contains(Keyword.ToUpper())
                                 select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_id);
                    }
                    else
                    {
                        lstObj = lstObj.OrderByDescending(m => m.customer_id);
                    }
                    break;
            }


            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }


        //
        // GET: /Admin/Customers/Details/5

        public ActionResult Details(int id = 0)
        {
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(new CustommerEditModel()
            {
                customer_id = id,
                customer_zipcode = customer.customer_zipcode,
                customer_state = customer.customer_state,
                customer_phone = customer.customer_phone,
                customer_address1 = customer.customer_address1,
                customer_address2 = customer.customer_address2,
                customer_block = customer.customer_block,
                customer_city = customer.customer_city,
                customer_company = customer.customer_company,
                customer_country = customer.customer_country,
                customer_email = customer.customer_email,
                customer_firstname = customer.customer_firstname,
                customer_lastname = customer.customer_lastname
            });
        }

        //
        // GET: /Admin/Customers/Create

        public ActionResult Create()
        {
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            return View();
        }

        //
        // POST: /Admin/Customers/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustommerModel c)
        {
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            if (ModelState.IsValid)
            {
                if ((from cc in db.customers where cc.customer_email.ToUpper().Equals(c.customer_email.ToUpper()) select cc).Count() > 0)
                {
                    ModelState.AddModelError("customer_email", "Email is already exist.");
                    return View(c);
                }
                else if (c.customer_password != c.customer_password_confirm)
                {
                    ModelState.AddModelError("customer_password_confirm", "Password confirm is not match.");
                    return View(c);
                }
                else
                {
                    db.customers.Add(new customer()
                    {
                        customer_address1 = c.customer_address1,
                        customer_address2 = c.customer_address2,
                        customer_block = false,
                        customer_city = c.customer_city,
                        customer_company = c.customer_company,
                        customer_country = c.customer_country,
                        customer_date = DateTime.Now,
                        customer_email = c.customer_email,
                        customer_firstname = c.customer_firstname,
                        customer_lastname = c.customer_lastname,
                        customer_password = c.customer_password,
                        customer_phone = c.customer_phone,
                        customer_state = c.customer_state,
                        customer_zipcode = c.customer_zipcode
                    });
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            
            return View(c);
        }

        //
        // GET: /Admin/Customers/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(new CustommerEditModel()
            {
                customer_id = id,
                customer_zipcode = customer.customer_zipcode,
                customer_state = customer.customer_state,
                customer_phone = customer.customer_phone,
                customer_address1 = customer.customer_address1,
                customer_address2 = customer.customer_address2,
                customer_block = customer.customer_block,
                customer_city = customer.customer_city,
                customer_company = customer.customer_company,
                customer_country = customer.customer_country,
                customer_email = customer.customer_email,
                customer_firstname = customer.customer_firstname,
                customer_lastname = customer.customer_lastname
            });
        }

        //
        // POST: /Admin/Customers/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustommerEditModel customer)
        {
            ViewBag.CountryList = new SelectList(db.countries, "country_name", "country_name");
            if (ModelState.IsValid)
            {
                db.Entry(
                    new customer()
                    {
                        customer_id = customer.customer_id,
                        customer_zipcode = customer.customer_zipcode,
                        customer_state = customer.customer_state,
                        customer_phone = customer.customer_phone,
                        customer_address1 = customer.customer_address1,
                        customer_address2 = customer.customer_address2,
                        customer_block = customer.customer_block,
                        customer_city = customer.customer_city,
                        customer_company = customer.customer_company,
                        customer_country = customer.customer_country,
                        customer_email = customer.customer_email,
                        customer_firstname = customer.customer_firstname,
                        customer_lastname = customer.customer_lastname
                    }).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        //
        // GET: /Admin/Customers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //Check customer
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View( customer);
           
        }

        //
        // POST: /Admin/Customers/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            customer customer = db.customers.Find(id);
            db.customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult ResetPassword(int id = 0, string returnUrl = "")
        {
            //Check id
            if (db.customers.Find(id) == null)
            {
                //Not found, error 404
                return HttpNotFound();
            }

            //Found, render reset password form
            return View(new CustomerResetPassword() { 
                id = id,
                ReturnUrl = returnUrl
            });

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(CustomerResetPassword c)
        {
            //Check ModelState
            if (ModelState.IsValid) //Valid
            {
                //Check comfirm Password
                if (c.ConfirmPassword != c.NewPassword) //Not match
                {
                    ModelState.AddModelError("ConfirmPassword", "Password confirm is not match.");
                    return View(c);
                }

                //Match
                
                //Get customer object from database
                var obj = db.customers.Find(c.id);

                //Assign new password
                obj.customer_password = c.ConfirmPassword;

                //Save
                db.SaveChanges();

                //Return url
                if (!string.IsNullOrEmpty(c.ReturnUrl))
                {
                    return Redirect(c.ReturnUrl);
                }

                //Back to index if not specify returnUrl
                return RedirectToAction("Index");
            }

            //ModelState is not valid
            return View(c);
        }
        public ActionResult Block(int id , string returnUrl = "")
        {
            //get order
            var o = db.orders.Where(m => m.customer_id == id && (m.orders_status.Equals("Pending") || m.orders_status.Equals("Shipping")));
            //Check order
            if (o.Count() > 0)
            {
                return View();
            }
            //block customer in local db
            db.customers.Find(id).customer_block = !db.customers.Find(id).customer_block;
            //save change in server
            db.SaveChanges();

            if (returnUrl != "")
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}