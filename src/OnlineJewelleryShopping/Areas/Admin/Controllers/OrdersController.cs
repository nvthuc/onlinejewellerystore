﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class OrdersController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Orders/
        #region Index Order
        //
        // GET: /Admin/Products/

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.orders.Remove(ctx.orders.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Index(string Status = "All", string Keyword = "", string Filter = "All", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Status
            ViewBag.CurrentStatus = Status;

            //Current Key word
            ViewBag.KeyWord = Keyword;

            //CurentFilter
            ViewBag.CurrentFilter = Filter;




            //All Order
            var lstObj = from c in db.orders select c;

            //Status module
            switch (Status)
            {
                case "Cancel":
                    lstObj = from i in lstObj where i.orders_status == "Cancel" select i;
                    break;
                case "Done":
                    lstObj = from i in lstObj where i.orders_status == "Done" select i;
                    break;
                case "Pending":
                    lstObj = from i in lstObj where i.orders_status == "Pending" select i;
                    break;
                case "Shipping":
                    lstObj = from i in lstObj where i.orders_status == "Shipping" select i;
                    break;
                default:
                    break;
            }

            //Search module
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.billing_firstname.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_lastname.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_company.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_address2.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_city.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_state.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_firstname.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_lastname.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_company.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_address2.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.billing_city.ToUpper().Contains(Keyword.ToUpper()) ||
                             i.shipping_state.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }

            //Filter module
            switch (Filter)
            {
                case "CustomerName":
                    lstObj = from i in lstObj
                             where
                                 i.billing_firstname.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.billing_lastname.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.shipping_firstname.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.shipping_lastname.ToUpper().Contains(Keyword.ToUpper())
                             orderby i.billing_firstname ascending
                             select i;
                    break;
                case "CustomerAddress":
                    lstObj = from i in lstObj
                             where
                                 i.billing_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.billing_address2.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.shipping_address1.ToUpper().Contains(Keyword.ToUpper()) ||
                                 i.shipping_address2.ToUpper().Contains(Keyword.ToUpper())
                             orderby i.billing_address1 ascending
                             select i;
                    break;

                default:
                    lstObj = from i in lstObj orderby i.orders_id descending select i;
                    break;
            }

            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion

        #region Change status

        public ActionResult ChangeStatus(int id, string NewStatus, string returnurl)
        {
            var obj = db.orders.Find(id);
            if (NewStatus == "Cancel")
            {
                foreach (var item in db.orders_detail.Where(m => m.orders_id == id))
                {
                    item.product.product_quantity += item.orders_detail_quantity;
                }
            }

            obj.orders_status = NewStatus;
            db.SaveChanges();

            return Redirect(returnurl);
        }
        #endregion
        #region Order details
        //
        // GET: /Admin/Orders/Details/5

        public ActionResult Details(int id = 0)
        {
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }
        [ChildActionOnly]
        public ActionResult ProductList(int id)
        {
            var lstPro = new List<OrderListProduct>();
            var lstOrderDetail = from i in db.orders_detail where i.orders_id == id select i;
            foreach (var item in lstOrderDetail)
            {
                lstPro.Add(new OrderListProduct()
                {
                    id = item.product_id,
                    name = item.product.product_name,
                    image = item.product.product_images_01,
                    price = (double)item.product.product_price,
                    discount = (double)item.orders_detail_discount,
                    quantity = (int)item.orders_detail_quantity
                   
                });
            }
            lstPro.OrderBy(m => m.name);
            ViewBag.VAT = db.orders.Find(id).orders_vat == null ? db.account_tant.First().account_tant_vat : db.orders.Find(id).orders_vat;
            //ViewBag.VAT = 10;
            ViewBag.OrderTotal = db.orders.FirstOrDefault(m => m.orders_id == id).orders_total;
            ViewBag.Model = lstPro;
            return View();
        }

        #endregion
        //
        // GET: /Admin/Orders/Create
        #region Create Order
        public ActionResult Create()
        {
            ViewBag.Country = CountryModel.GetCountry();
            return View();
        }

        //
        // POST: /Admin/Orders/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderModel o)
        {
            if (ModelState.IsValid)
            {
                db.orders.Add(new order()
                {
                    orders_payment = o.orders_payment,
                    orders_status = "Pending",
                    billing_address1 = o.billing_address1,
                    billing_lastname = o.billing_lastname,
                    billing_phone = o.billing_lastname,
                    billing_state = o.billing_state,
                    billing_zipcode = o.billing_zipcode,
                    billing_firstname = o.billing_firstname,
                    billing_address2 = o.billing_address2,
                    billing_city = o.billing_city,
                    billing_company = o.billing_company,
                    billing_country = o.billing_country,

                    shipping_address1 = o.shipping_address1,
                    shipping_lastname = o.shipping_lastname,
                    shipping_phone = o.shipping_lastname,
                    shipping_state = o.shipping_state,
                    shipping_zipcode = o.shipping_zipcode,
                    shipping_firstname = o.shipping_firstname,
                    shipping_address2 = o.shipping_address2,
                    shipping_city = o.shipping_city,
                    shipping_company = o.shipping_company,
                    shipping_country = o.shipping_country,


                    orders_comment = o.orders_comment,
                    orders_vat = db.account_tant.FirstOrDefault().account_tant_vat,
                    orders_date = DateTime.Today
                });
                db.SaveChanges();
                return RedirectToAction("AddProduct", new { id = db.orders.OrderByDescending(m => m.orders_id).FirstOrDefault().orders_id });
            }

            ViewBag.Country = CountryModel.GetCountry();
            return View(o);
        }
        #endregion

        #region Add product to sale off
        [HttpPost]
        public ActionResult AddProduct(int id, FormCollection collection)
        {
            //Order id 
            int OrderId = id;

            //Find in collection
            foreach (var key in collection.AllKeys)
            {
                if (!string.IsNullOrEmpty(collection[key.ToString()]) && int.Parse(collection[key.ToString()].ToString()) > 0)
                {
                    //Product have quantity
                    //Product id
                    int pid = int.Parse(key.ToString());

                    //Product Price
                    double price = (double)db.products.Find(pid).product_price;

                    //Quantity
                    int quantity = int.Parse(collection[key.ToString()]);

                    //Discount
                    double discount = GetDiscount(pid);

                    //Update product quantity
                    UpdateProductQuantity(pid, quantity);

                    //Add order detail
                    db.orders_detail.Add(new orders_detail()
                    {
                        product_id = pid,
                        orders_detail_price = price,
                        orders_id = OrderId,
                        orders_detail_quantity = quantity,
                        orders_detail_discount = discount
                    });
                    //save
                    db.SaveChanges();
                }
            }
            ReCaculateOrder(OrderId);
            return RedirectToAction("AddProduct", new { id = id });
        }
        public ActionResult AddProduct(int id, string Keyword = "", int Pagenumber = 1, int PageSize = 10, int Brand = 0, int Category = 0)
        {
            //Find sale off in database 
            var objSaleOff = db.orders.Find(id);

            //Check result
            if (objSaleOff == null) //Not found data
            {
                return HttpNotFound();
            }

            //Found saleOff==========================================================================================

            //Current ID
            ViewBag.CurrentID = id.ToString();

            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Brand Dropdownlist
            ViewBag.Brands = BrandModel.GetFilter();
            //Current Brand
            ViewBag.CurrentBrand = Brand.ToString();

            //Category Dropdownlist
            ViewBag.Categories = CategoryModel.GetFilter();
            //Current Category
            ViewBag.CurrentCategory = Category.ToString();

            //List product ID in order
            var lstPID = from p in db.orders_detail where p.orders_id == id select p.product_id;

            //All product not in sale off
            var lstObj = from c in db.products where !lstPID.Contains(c.product_id) && c.product_status == true select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }
            //Brand filter
            if (Brand != 0)
            {
                lstObj = from i in lstObj where i.brand_id == Brand orderby i.brand.brand_name ascending select i;
            }

            //Category filter
            if (Category != 0)
            {
                lstObj = from i in lstObj
                         where i.category_id == Category ||
                             (from c in db.categories where c.category_parent == Category select c.category_id).Contains((int)i.category_id)
                         orderby i.category.category_name ascending
                         select i;
            }
            if (Brand == 0 && Category == 0)
            {
                lstObj = from i in lstObj orderby i.product_name ascending select i;
            }
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion

        #region Remove product to sale off
        [HttpPost]
        public ActionResult EditProduct(int id, FormCollection collection)
        {
            //Order id 
            int OrderId = id;

            //Find in collection
            foreach (var key in collection.AllKeys)
            {
                if (int.Parse(collection[key.ToString()].ToString()) > 0)
                {
                    //Product have quantity
                    //Product id
                    int pid = int.Parse(key.ToString());


                    //Quantity
                    int Newquantity = int.Parse(collection[key.ToString()]);
                    int OldQ = (int)db.orders_detail.First(m => m.orders_id == id && m.product_id == pid).orders_detail_quantity;

                    //Update product quantity
                    UpdateProductQuantity(pid, Newquantity - OldQ);

                    //Update order detail
                    var o = db.orders_detail.First(m => m.orders_id == id && m.product_id == pid);
                    o.orders_detail_quantity = Newquantity;

                    //save
                    db.SaveChanges();
                }
                else
                {
                    int pid = int.Parse(key.ToString());
                    db.Entry(db.orders_detail.First(m => m.orders_id == id && m.product_id == pid)).State = EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            ReCaculateOrder(OrderId);
            return RedirectToAction("EditProduct", new { id = id });
        }
        public ActionResult EditProduct(int id, string Keyword = "", int Pagenumber = 1, int PageSize = 10, int Brand = 0, int Category = 0)
        {
            //Find sale off in database 
            var objSaleOff = db.orders.Find(id);

            //Check result
            if (objSaleOff == null) //Not found data
            {
                return HttpNotFound();
            }

            //Found saleOff==========================================================================================

            //Current ID
            ViewBag.CurrentID = id.ToString();
            ViewBag.ID = id;
            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Brand Dropdownlist
            ViewBag.Brands = BrandModel.GetFilter();
            //Current Brand
            ViewBag.CurrentBrand = Brand.ToString();

            //Category Dropdownlist
            ViewBag.Categories = CategoryModel.GetFilter();
            //Current Category
            ViewBag.CurrentCategory = Category.ToString();

            //List product ID in order
            var lstPID = from p in db.orders_detail where p.orders_id == id select p.product_id;

            //All product not in sale off
            var lstObj = from c in db.products where lstPID.Contains(c.product_id) && c.product_status == true select c;

            //Search 
            if (!string.IsNullOrEmpty(Keyword))
            {
                lstObj = from i in lstObj
                         where
                             i.product_name.ToUpper().Contains(Keyword.ToUpper())
                         select i;
            }
            //Brand filter
            if (Brand != 0)
            {
                lstObj = from i in lstObj where i.brand_id == Brand orderby i.brand.brand_name ascending select i;
            }

            //Category filter
            if (Category != 0)
            {
                lstObj = from i in lstObj
                         where i.category_id == Category ||
                             (from c in db.categories where c.category_parent == Category select c.category_id).Contains((int)i.category_id)
                         orderby i.category.category_name ascending
                         select i;
            }
            if (Brand == 0 && Category == 0)
            {
                lstObj = from i in lstObj orderby i.product_name ascending select i;
            }
            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }
        #endregion
        //GET: /Admin/Orders/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //find in database
            order o = db.orders.Find(id);
            if (o == null) //Not found data
            {
                //alert 404
                return HttpNotFound();
            }
            //-----------Found data
            //data for country dropdown list
            ViewBag.Country = CountryModel.GetCountry();
            //data for product list

            return View(new OrderModel()
            {
                billing_zipcode = o.billing_zipcode,
                billing_country = o.billing_country,
                billing_city = o.billing_city,
                billing_state = o.billing_state,
                billing_address2 = o.billing_address2,
                billing_address1 = o.billing_address1,
                billing_firstname = o.billing_firstname,
                billing_lastname = o.billing_lastname,
                billing_phone = o.billing_phone,
                customer_id = o.customer_id,
                orders_date = o.orders_date,
                orders_id = o.orders_id,
                orders_total = (float)o.orders_total,
                orders_vat = (float)o.orders_vat,
                shipping_address1 = o.shipping_address1,
                shipping_address2 = o.shipping_address2,
                shipping_city = o.shipping_city,
                shipping_country = o.shipping_city,
                shipping_firstname = o.shipping_firstname,
                shipping_lastname = o.shipping_lastname,
                shipping_phone = o.shipping_phone,
                shipping_zipcode = o.shipping_zipcode,
                orders_comment = o.billing_company
            });
        }

        //
        // POST: /Admin/Orders/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OrderModel o)
        {
            if (ModelState.IsValid)
            {
                var oo = db.orders.Find(o.orders_id);
                oo.billing_zipcode = o.billing_zipcode;
                oo.billing_country = o.billing_country;
                oo.billing_city = o.billing_city;
                oo.billing_state = o.billing_state;
                oo.billing_address2 = o.billing_address2;
                oo.billing_address1 = o.billing_address1;
                oo.billing_firstname = o.billing_firstname;
                oo.billing_lastname = o.billing_lastname;
                oo.billing_phone = o.billing_phone;
                oo.customer_id = o.customer_id;
                oo.orders_date = o.orders_date;
                oo.orders_total = o.orders_total;
                oo.orders_vat = o.orders_vat;
                oo.shipping_address1 = o.shipping_address1;
                oo.shipping_address2 = o.shipping_address2;
                oo.shipping_city = o.shipping_city;
                oo.shipping_country = o.shipping_city;
                oo.shipping_firstname = o.shipping_firstname;
                oo.shipping_lastname = o.shipping_lastname;
                oo.shipping_phone = o.shipping_phone;
                oo.shipping_zipcode = o.shipping_zipcode;
                oo.orders_comment = o.orders_comment;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Country = CountryModel.GetCountry();
            return View(o);
        }

        //
        // GET: /Admin/Orders/Delete/5

        public ActionResult Delete(int id = 0)
        {
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            //Update product quantity
            foreach (var item in db.orders_detail.Where(m => m.orders_id == id))
            {
                //update product
                db.products.First(m => m.product_id == item.product_id).product_quantity += item.orders_detail_quantity;

                //delete order detail
                db.orders_detail.Remove(item);

            }
           
            //delete order
            db.orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // POST: /Admin/Orders/Delete/5


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        #region Function
        public double GetDiscount(int pid)
        {
            double discount = 0;
            //Check discount
            var dis = from i in db.product_discount
                      where
                          (
                          from a in db.product_discount_detail
                          where
                              a.product_id == pid
                          select a.product_id
                          ).Contains(i.product_discount_id)
                          &&
                          i.product_discount_startdate <= DateTime.Today
                          &&
                          i.product_discount_enddate >= DateTime.Today
                      select i;
            if (dis.Count() > 0)
            {
                discount = (double)dis.First().product_discount_amount;
            }
            return discount;
        }

        public void UpdateProductQuantity(int pid, int q)
        {
            var obj = db.products.Find(pid);
            obj.product_quantity = obj.product_quantity - q;
            db.SaveChanges();
        }
        public void ReCaculateOrder(int oid)
        {
            var order = db.orders.Find(oid);

            double total = 0;
            foreach (var item in order.orders_detail)
            {
                total += ((double)item.orders_detail_price * (double)item.orders_detail_quantity) - (((double)item.orders_detail_price * (double)item.orders_detail_quantity) * ((double)item.orders_detail_discount) / 100);
            }
            total = total + total * ((double)order.orders_vat) / 100;
            order.orders_total = total;
            db.SaveChanges();
        }
        #endregion

    }
}