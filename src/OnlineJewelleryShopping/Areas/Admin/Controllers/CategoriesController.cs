﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class CategoriesController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Categories/

        public ActionResult Index(string Keyword = "", string Order = "desc", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;


            //Current Order
            ViewBag.Order = Order;

            //Get All category
            var ls = from i in db.categories select i;

            //Search Module
            if (Keyword !="")
            {
                ls = from i in ls
                     where
                         i.category_name.ToUpper().Contains(Keyword.ToUpper()) ||
                        (from c in db.categories where c.category_id == i.category_parent && c.category_name.ToUpper().Contains(Keyword.ToUpper()) select c.category_name).Count() > 0
                     select i;
            }



            //init list model
            var lstCategory = new List<CategoryModel>();

            //get top level in source data
            var dsTopLevel = from i in ls where i.category_parent == null select i;

            //add top level to list model
            foreach (category toplevel in dsTopLevel)
            {
                lstCategory.Add(new CategoryModel()
                {
                    category_name = toplevel.category_name,
                    category_image = toplevel.category_image,
                    category_id = toplevel.category_id,
                    IsDelete = toplevel.products.Count() == 0 ? true : false
                });

                //find sencond level
                var dsSecondLevel = ls.Where(m => m.category_parent == toplevel.category_id);

                //Add second level to list with "--" before name
                foreach (var subLevel in dsSecondLevel)
                {
                    lstCategory.Add(new CategoryModel()
                    {
                        category_name = "-->" + subLevel.category_name,
                        category_image = subLevel.category_image,
                        category_id = subLevel.category_id
                    });
                }
            }

            //get second level in source data
            var lsSecond = from i in ls where i.category_parent != null select i;
            var re = from i in lsSecond where dsTopLevel.Where(m => m.category_id == i.category_parent).Count() == 0 select i;
            foreach (var subLevel in re)
            {
                lstCategory.Add(new CategoryModel()
                {

                    category_name = db.categories.First(m => m.category_id == subLevel.category_parent).category_name + "-->" + subLevel.category_name,
                    category_image = subLevel.category_image,
                    category_id = subLevel.category_id
                });
            }

            //Order module

            switch (Order)
            {
                case "asc":
                    lstCategory.OrderBy(m => m.category_name);
                    break;
                default:
                    lstCategory.OrderByDescending(m => m.category_name);
                    break;
            }
            return View(lstCategory.ToPagedList(Pagenumber, PageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.categories.Remove(ctx.categories.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        //
        // GET: /Admin/Categories/Details/5

        public ActionResult Details(int id = 0)
        {
            //Find in database
            category category = db.categories.Find(id);
            if (category == null) // not found
            {
                return HttpNotFound();
            }
            //found
            ViewBag.Category1 = "";
            if (category.category_parent != null) //check parent category
            {   //sub category
                //select parent category
                var c = db.categories.Where(m => m.category_id == category.category_parent).FirstOrDefault();
                //set viewBag
                ViewBag.Category1 = c.category_name == null ? "" : c.category_name;
            }
            else  //top category
            {
                //get list sub category
                var ls = db.categories.Where(m => m.category_parent == category.category_id).ToList();
                //asign Viewbag
                foreach (var item in ls)
                {
                    ViewBag.Category1 += item.category_name + ", ";
                }
            }

            // ViewBag.NumberOfProduct
            ViewBag.NumberOfProduct = 0;
            if (category.category_parent != null)
            {
                ViewBag.NumberOfProduct = db.products.Where(p => p.category_id == category.category_id).Count();
            }
            else
            {
                foreach (var item in db.categories.Where(m => m.category_parent == category.category_id))
                {
                    ViewBag.NumberOfProduct += db.products.Where(p => p.category_id == item.category_id).Count();
                }
            }
            return View(category);
        }

        //
        // GET: /Admin/Categories/Create

        public ActionResult Create()
        {
            var lst = CategoryModel.GetTopLevel();

            ViewBag.TopCategory = lst;
            return View();
        }

        //
        // POST: /Admin/Categories/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryModel c)
        {

            if (ModelState.IsValid)
            {
                db.categories.Add(new category()
                {
                    category_descripttion = c.category_descripttion,
                    category_image = c.category_image,
                    category_name = c.category_name,
                    category_parent = c.category_parent
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TopCategory = CategoryModel.GetTopLevel();
            return View(c);
        }

        //
        // GET: /Admin/Categories/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in server
            category c = db.categories.Find(id);
            if (c == null)
            {
                //Not found
                return HttpNotFound();
            }
            //Found
            //model for dropdown list
            ViewBag.category_parent = CategoryModel.GetTopLevel();
            //render view
            return View(new CategoryModel()
            {
                category_id = c.category_id,
                category_descripttion = c.category_descripttion,
                category_image = c.category_image,
                category_name = c.category_name,
                category_parent = c.category_parent
            });
        }

        //
        // POST: /Admin/Categories/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryModel c)
        {
            //check model
            if (ModelState.IsValid) //model is valid
            {
                //get category from database
                var cc = db.categories.Find(c.category_id);
                cc.category_parent = c.category_parent;
                cc.category_name = c.category_name;
                cc.category_image = c.category_image;
                cc.category_descripttion = c.category_descripttion;
                //save to server
                db.Entry(cc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.category_parent = CategoryModel.GetTopLevel();
            return View(c);
        }

        //
        // GET: /Admin/Categories/Delete/5

        public ActionResult Delete(int id = 0)
        {
            category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        //
        // POST: /Admin/Categories/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            category category = db.categories.Find(id);
            if (category.products.Count() >0 || db.categories.Where(m => m.category_parent == id).Count() > 0)
            {
                return HttpNotFound();
            }
            db.categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}