﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class AController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Advertises/

        public ActionResult Index(string Position = "All", int PageNumber = 1, int PageSize = 5)
        {
            //Data for position
            ViewBag.Position = Position;

            //All adv
            var lstObj = from a in db.advertises select a;
            switch (Position)
            {
                case "Center":
                    lstObj = from a in lstObj where a.advertise_name.Equals("Center") orderby a.advertise_id descending select a;
                    break;
                case "Footer":
                    lstObj = from a in lstObj where a.advertise_name.Equals("Footer") orderby a.advertise_id descending select a;
                    break;
                case "Product":
                    lstObj = from a in lstObj where a.advertise_name.Equals("Product") orderby a.advertise_id descending select a;
                    break;
                default:
                    lstObj = from a in lstObj orderby a.advertise_id descending select a;
                    break;
            }
            //Render View
            return View(lstObj.ToPagedList(PageNumber, PageSize));
        }
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.advertises.Remove(ctx.advertises.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        //
        // GET: /Admin/Advertises/Details/5

        public ActionResult Details(int id = 0)
        {
            //Find in database
            advertise advertise = db.advertises.Find(id);
            //Check result
            if (advertise == null) //Not found
            {
                return HttpNotFound();
            }
            //Found
            //Render View
            return View(advertise);
        }

        //
        // GET: /Admin/Advertises/Create

        public ActionResult Create()
        {
            ////Data for dropdownlist target
            ViewBag.Target = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "_self", Value = "_self" }, new SelectListItem() { Text = "_blank", Value = "_blank" } }, "Value", "Text");
            ////Data for position
            ViewBag.Position = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Center", Value = "Center" }, new SelectListItem() { Text = "Product", Value = "Product" }, new SelectListItem() { Text = "Footer", Value = "Footer" } }, "Value", "Text");
            ////Render View
            return View();
        }

        //
        // POST: /Admin/Advertises/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdvModel adv)
        {
            //ViewBag Target
            if (ModelState.IsValid)
            {
                //Create instance of advertise
                var a = new advertise();
                //Assign properties for advertise 
                a.advertise_date = DateTime.Now;
                a.advertise_image = adv.advertise_image;
                a.advertise_name = adv.advertise_name;
                a.advertise_target = adv.advertise_target;
                a.advertise_url = adv.advertise_url;
                //Add to context database
                db.advertises.Add(a);
                //Save to database server
                db.SaveChanges();
                //Back to Index Action
                return RedirectToAction("Index");
            }
            ////Data for dropdownlist target
            ViewBag.Target = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "_self", Value = "_self" }, new SelectListItem() { Text = "_blank", Value = "_blank" } }, "Value", "Text");
            ////Data for position
            ViewBag.Position = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Center", Value = "Center" }, new SelectListItem() { Text = "Product", Value = "Product" }, new SelectListItem() { Text = "Footer", Value = "Footer" } }, "Value", "Text");
            return View(adv);
        }

        //
        // GET: /Admin/Advertises/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Find in database
            advertise advertise = db.advertises.Find(id);
            if (advertise == null) //Not Found
            {
                return HttpNotFound();
            }
            //Found
            //Data for dropdownlist target
            ViewBag.Target = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "_self", Value = "_self" }, new SelectListItem() { Text = "_blank", Value = "_blank" } }, "Value", "Text");
            //Data for position
            ViewBag.Position = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Center", Value = "Center" }, new SelectListItem() { Text = "Product", Value = "Product" }, new SelectListItem() { Text = "Footer", Value = "Footer" } }, "Value", "Text");
            //File Path
            //Render View
            return View(
                new AdvModel()
                {
                    advertise_url = advertise.advertise_url,
                    advertise_target = advertise.advertise_target,
                    advertise_name = advertise.advertise_name,
                    advertise_image = advertise.advertise_image,
                    advertise_id = advertise.advertise_id
                }
            );
        }

        //
        // POST: /Admin/Advertises/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdvModel obj)
        {
            //Check ModelSate
            if (ModelState.IsValid)
            {
                //Get object from datbase
                var adv = db.advertises.Find(obj.advertise_id);

                //Check null object
                if (adv == null)
                {
                    return HttpNotFound();
                }

                //Object not null
                //Assign new properties value
                adv.advertise_url = obj.advertise_url;
                adv.advertise_target = obj.advertise_target;
                adv.advertise_name = obj.advertise_name;

                //Image
                //Check new value of image
                if (!obj.advertise_image.ToUpper().Equals(adv.advertise_image.ToUpper()))
                {
                    //Changed image
                    string oldImage = adv.advertise_image;
                    string newImage = obj.advertise_image;
                    //Delete old image
                    System.IO.File.Delete(Server.MapPath(adv.advertise_image));
                    //Asign new image
                    adv.advertise_image = obj.advertise_image;
                } // Not changed image. Do not thing obout image field

                //Mark object State as Modified
                db.Entry(adv).State = EntityState.Modified;

                //Save to database server
                db.SaveChanges();

                //Redirect to index action
                return RedirectToAction("Index");
            }

            //ModelState is invalid

            //Data for dropdownlist target
            //ViewBag.Target = TargetSelectList();

            ////Data for position
            //ViewBag.Position = PositionSelectListCreate();

            //Back to Edit View
            return View(obj);
        }

        //
        // GET: /Admin/Advertises/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //Find in database
            advertise advertise = db.advertises.Find(id);
            //Check result 
            if (advertise == null) //Not Found
            {
                return HttpNotFound();
            }
            //Found
            //Render View
            return View(advertise);
        }

        //
        // POST: /Admin/Advertises/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Find in database
            advertise advertise = db.advertises.Find(id);
            //delete image
            System.IO.File.Delete(Server.MapPath(advertise.advertise_image));
            //Remove
            db.advertises.Remove(advertise);
            //Save
            db.SaveChanges();


            //Redirect to index action
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}