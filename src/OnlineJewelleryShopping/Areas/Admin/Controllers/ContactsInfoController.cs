﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class ContactsInfoController : Controller
    {
        //
        // GET: /Admin/ContactsInfo/

        public ActionResult Index()
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                //Get data from database
                var contact = ctx.contact_info.FirstOrDefault();
                //Check result
                if (contact == null)
                {
                    //No data
                    //Init new contact
                    contact = new contact_info()
                    {
                        contact_info_address = "Address",
                        contact_info_email = "example@hostmail.com",
                        contact_info_fax = "0123585456",
                        contact_info_phone = "01235845658",
                        contact_info_skype = "skypeid"
                    };

                    //Add to database context
                    ctx.contact_info.Add(contact);

                    //Save to database
                    ctx.SaveChanges();

                    //Render view with contact model
                    return View(new ContactModel()
                    {
                        address = contact.contact_info_address,
                        email = contact.contact_info_email,
                        fax = contact.contact_info_fax,
                        phone = contact.contact_info_phone,
                        skype = contact.contact_info_skype
                    });
                }
                else //Have record in database
                {
                    //Render view with this model
                    return View(new ContactModel()
                    {
                        address = contact.contact_info_address,
                        email = contact.contact_info_email,
                        fax = contact.contact_info_fax,
                        phone = contact.contact_info_phone,
                        skype = contact.contact_info_skype
                    });
                }
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactModel c)
        {
            //Check model state
            if (ModelState.IsValid) //Valid
            {
                using (var ctx = new OnlineJewelleryStoreEntities())
                {
                    //Get record from database
                    var cc = ctx.contact_info.FirstOrDefault();

                    //Asign new value
                    cc.contact_info_address = c.address;
                    cc.contact_info_email = c.email;
                    cc.contact_info_fax = c.fax;
                    cc.contact_info_phone = c.phone;
                    cc.contact_info_skype = c.skype;

                    //Save to database
                    ctx.Entry(cc).State = System.Data.EntityState.Modified;
                    ctx.SaveChanges();
                    //Redirect to Contact view
                    return RedirectToAction("Index");
                }
            }

            //Invalid
            //Return view to display error message
            return View(c);

        }
        public ActionResult Edit()
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                //Get data from database
                var contact = ctx.contact_info.FirstOrDefault();
                //Check result
                if (contact == null)
                {
                    //No data
                    //Init new contact
                    contact = new contact_info()
                    {
                        contact_info_address = "Address",
                        contact_info_email = "example@hostmail.com",
                        contact_info_fax = "0123585456",
                        contact_info_phone = "01235845658",
                        contact_info_skype = "skypeid"
                    };

                    //Add to database context
                    ctx.contact_info.Add(contact);

                    //Save to database
                    ctx.SaveChanges();

                    //Render view with contact model
                    return View(new ContactModel()
                    {
                        address = contact.contact_info_address,
                        email = contact.contact_info_email,
                        fax = contact.contact_info_fax,
                        phone = contact.contact_info_phone,
                        skype = contact.contact_info_skype
                    });
                }
                else //Have record in database
                {
                    //Render view with this model
                    return View(new ContactModel()
                    {
                        address = contact.contact_info_address,
                        email = contact.contact_info_email,
                        fax = contact.contact_info_fax,
                        phone = contact.contact_info_phone,
                        skype = contact.contact_info_skype
                    });
                }
            }
        }
    }
}
