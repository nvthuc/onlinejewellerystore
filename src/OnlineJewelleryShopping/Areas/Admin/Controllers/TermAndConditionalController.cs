﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;

namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class TermAndConditionalController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/TermAndConditional/

        public ActionResult Index()
        {
            var obj = db.term_and_conditional.FirstOrDefault();
            if (obj != null)
            {
                return View(obj);

            }
            obj = new term_and_conditional(){term_and_conditional_conten = "No data"};
            db.term_and_conditional.Add(obj);
            db.SaveChanges();
            return View(obj);
        }

       

        
        
        //
        // GET: /Admin/TermAndConditional/Edit/5
        
        public ActionResult Edit()
        {
            term_and_conditional term_and_conditional = db.term_and_conditional.FirstOrDefault();
            if (term_and_conditional == null)
            {
                term_and_conditional = new term_and_conditional() {  term_and_conditional_conten = "No data"};
                db.term_and_conditional.Add(term_and_conditional);
                db.SaveChanges();
                return View(term_and_conditional);
            }
            return View(term_and_conditional);
        }

        //
        // POST: /Admin/TermAndConditional/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(term_and_conditional term_and_conditional)
        {
            var obj = db.term_and_conditional.FirstOrDefault();
            if (obj != null)
            {
                obj.term_and_conditional_conten = term_and_conditional.term_and_conditional_conten;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            obj = new term_and_conditional() { term_and_conditional_conten = term_and_conditional.term_and_conditional_conten };
            db.term_and_conditional.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}