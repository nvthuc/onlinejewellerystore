﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        #region Index admin
        //
        // GET: /Admin/Admin/

        public ActionResult Index(string Sort, string Keyword, int PageNumber = 1, int PageSize = 10)
        {
            //Sort parameter
            ViewBag.NameParameter = string.IsNullOrEmpty(Sort) ? "Name_desc" : "";
            ViewBag.EmailParameter = Sort == "Email" ? "Email_desc" : "Email";
            ViewBag.CurrentSort = Sort != null ? Sort : "";

            //Search parameter
            ViewBag.Keyword = string.IsNullOrEmpty(Keyword) ? "" : Keyword;

            //Page parameter
            ViewBag.PageNumber = PageNumber;

            //Using context to get list object
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                //Get all list admin 
                var dbObj = from s in ctx.admin_login select s;

                //Search module
                //Key word
                ViewBag.Keyword = Keyword;
                if (!string.IsNullOrEmpty(Keyword))
                {
                    dbObj = from s in dbObj where s.admin_fullname.ToUpper().Contains(Keyword.ToUpper()) || s.admin_username.ToUpper().Contains(Keyword.ToUpper()) select s;
                }
                //Sort module
                switch (Sort)
                {
                    case "Email": //order desc by email
                        dbObj = dbObj.OrderByDescending(m => m.admin_username);
                        break;
                    case "Email_desc": //order asc by email
                        dbObj = dbObj.OrderBy(m => m.admin_username);
                        break;
                    case "Name_desc": //order desc by name
                        dbObj = dbObj.OrderByDescending(m => m.admin_fullname);
                        break;
                    default: //default order asc by name
                        dbObj = dbObj.OrderBy(m => m.admin_fullname);
                        break;
                }

                //Render view
                return View(dbObj.ToPagedList(pageNumber: PageNumber, pageSize: PageSize));
            }
        }
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.admin_login.Remove(ctx.admin_login.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Create new admin
        //Add New Admin: /Admin/Admin/AddNew
        public ActionResult Create()
        {
            //Render form ceate
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdminCreateModel model)
        {
            //Check modelState
            if (ModelState.IsValid) //Valid
            {
                using (var ctx = new OnlineJewelleryStoreEntities())
                {
                    //Check password
                    if (model.Password != model.ConfirmPassword)
                    {
                        ModelState.AddModelError("ConfirmPassword", "Password confirm is not match.");
                        return View(model);
                    }
                    //Check Email 
                    var lst = ctx.admin_login.Where(a => a.admin_username == model.Email).ToList();
                    if (lst.Count > 0) //Email aready exist
                    {
                        ModelState.AddModelError("Email", "Email aready exist.");
                    }
                    else //New email
                    {
                        //Create admin object
                        admin_login adm = new admin_login();

                        //Assign properties
                        adm.admin_fullname = model.FullName;
                        adm.admin_phone = model.Phone;
                        adm.admin_password = model.Password;
                        adm.admin_username = model.Email;
                        adm.admin_block = false;
                        //Save to database
                        ctx.admin_login.Add(adm);
                        ctx.SaveChanges();

                        //Redirect to index action
                        return RedirectToAction("Index", "Admin", new { area = "Admin" });
                    }
                }
            }

            //Invalid
            //Return view to display errror messages
            return View(model);
        }
        #endregion

        #region Edit admin info
        //Edit info : /Admin/Admin/Edit/1
        public ActionResult Edit(int id = 0)
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                var adm = ctx.admin_login.Find(id);
                if (adm != null)
                {
                    var admin = new AdminEditModel();
                    admin.Email = adm.admin_username;
                    admin.FullName = adm.admin_fullname;
                    admin.Phone = adm.admin_phone;
                    admin.ID = adm.admin_id;
                    return View(admin);
                }
                else
                {
                    return HttpNotFound();
                }
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var ctx = new OnlineJewelleryStoreEntities())
                    {
                        var obj = ctx.admin_login.Find(model.ID);
                        obj.admin_fullname = model.FullName;
                        obj.admin_username = model.Email;
                        obj.admin_phone = model.Phone;
                        ctx.Entry(obj).State = System.Data.EntityState.Modified;
                        ctx.SaveChanges();
                        return RedirectToAction("Index", "Admin");
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View(model);

        }
        #endregion

        #region  Delete admin
        public ActionResult Delete(int id)
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                ctx.Entry(ctx.admin_login.Find(id)).State = System.Data.EntityState.Deleted;
                ctx.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }

        }
        #endregion

        #region Reset Password
        public ActionResult ResetPassword(int id)
        {
            return View(new AdminResetPassModel() { ID = id });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(AdminResetPassModel model)
        {
            //check model
            if (ModelState.IsValid) //Valid
            {
                //check password
                if (model.NewPassword.Equals(model.ConfirmPassword)) //Match
                {
                    using (var ctx = new OnlineJewelleryStoreEntities())
                    {
                        //Get admin in database
                        var obj = ctx.admin_login.Find(model.ID);
                        //Set new password
                        obj.admin_password = model.NewPassword;

                        //Mark obj admin as modified
                        ctx.Entry(obj).State = System.Data.EntityState.Modified;

                        //Save to database
                        ctx.SaveChanges();

                        //Redirect to index
                        return RedirectToAction("Index", "Admin");
                    }
                }

                //Not match
                ModelState.AddModelError("ConfirmPassword", "Password confirm is not match.");
                return View(model);
            }

            //Invalid
            //Return view and display error message
            return View(model);

        }
        #endregion

        #region Login
        public ActionResult Login()
        {
            int id = 0;
            //Check cookies 
            if (Request.Cookies["AdminID"] != null && int.TryParse(Request.Cookies["AdminID"].Value, out id) && Request.Cookies["AdminName"] != null && Request.Cookies["IsAuthorized"] != null && Request.Cookies["IsAuthorized"].Value == "true")
            {
                using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
                {
                    if (ctx.admin_login.Find(id) != null && ctx.admin_login.Find(id).admin_fullname == Request.Cookies["AdminName"].Value)
                    {
                        //Set session
                        Session["AdminID"] = id;
                        Session["AdminName"] = Request.Cookies["AdminName"].Value;
                        Session["IsAuthorized"] = true;
                        //Go te admin manager page
                        return RedirectToAction("Index", "Orders");
                    }
                }
            }
            //Render login form
            return View();
        }

        [HttpPost]
        public ActionResult Login(OnlineJewelleryShopping.Areas.Admin.Models.LoginModel model)
        {
            //Check model state
            if (ModelState.IsValid) //Valid
            {
                using (var ctx = new OnlineJewelleryStoreEntities())
                {
                    //Get in database
                    var a = ctx.admin_login.FirstOrDefault(m => m.admin_username == model.Email && m.admin_password == model.Password);

                    //Check result
                    if (a == null) // not found
                    {
                        ViewBag.Message = "Email or password is not valid.";
                        return View(model);
                    }
                    else if (a.admin_block == true)
                    {
                        ViewBag.Message = "Account is blocked.Please contact administrator.";
                        return View(model);
                    }
                    else //Valid
                    {
                        //Set session
                        Session["AdminID"] = a.admin_id.ToString();
                        Session["AdminName"] = a.admin_fullname.ToString();
                        Session["IsAuthorized"] = true;

                        //Check keep login
                        if (model.IsRemember)
                        {
                            Response.Cookies.Add(new HttpCookie("AdminID", a.admin_id.ToString()));
                            Response.Cookies.Add(new HttpCookie("AdminName", a.admin_fullname));
                            Response.Cookies.Add(new HttpCookie("IsAuthorized", "true"));
                        }

                        //Redirect to admin controller
                        return RedirectToAction("Index", "Orders", new { area = "Admin" });
                    }
                }
            }

            //Invalid
            return View(model);
        }
        #endregion

        #region Logout
        [HttpPost]
        public ActionResult Logout()
        {
            //Remove session
            Session["AdminID"] = null;
            Session["AdminName"] = null;
            Session["IsAuthorized"] = null;

            //Remove cookies
            if (Request.Cookies["AdminID"] != null)
            {
                HttpCookie myCookie = new HttpCookie("AdminID");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["AdminName"] != null)
            {
                HttpCookie myCookie = new HttpCookie("AdminName");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["IsAuthorized"] != null)
            {
                HttpCookie myCookie = new HttpCookie("IsAuthorized");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            //Redirect to Login action
            return RedirectToAction("Login");
        }
        #endregion

        #region Fogot Password
        public ActionResult Forgot()
        {
            //Redirect to login action
            return RedirectToAction("Login");
        }
        #endregion

        #region Admin Details
        public ActionResult Details(int id = 0)
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                //Find in database
                var model = ctx.admin_login.Find(id);
                //check result
                if (model != null)
                {
                    //Render view
                    return View(model);
                }
                else //not found data
                {
                    return HttpNotFound();
                }
            }

        }
        #endregion

        #region Block admin
        public ActionResult Block(int id = 0, string url = "")
        {
            using (var ctx = new OnlineJewelleryStoreEntities())
            {
                //get obj from database
                var obj = ctx.admin_login.Find(id);

                //Check result
                if (obj != null) //Found
                {
                    //change block 
                    obj.admin_block = obj.admin_block == true ? false : true;
                    //Save 
                    ctx.Entry(obj).State = System.Data.EntityState.Modified;
                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                    //return Redirect(url);
                }
                else //Not found
                {
                    return RedirectToAction("Index");

                    //return Redirect(url);
                }
            }
        }
        #endregion
    }
}
