﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;
using OnlineJewelleryShopping.Areas.Admin.Models;

using PagedList;
namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class BrandsController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/Brands/

        // GET: /Admin/Contacts/
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            using (var ctx = new OnlineJewelleryShopping.Models.OnlineJewelleryStoreEntities())
            {
                foreach (string item in collection.Keys)
                {
                    int id = int.Parse(item);
                    ctx.brands.Remove(ctx.brands.Find(id));
                }
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult Index(string Keyword = "", string Filter = "All", string Order = "desc", int Pagenumber = 1, int PageSize = 10)
        {
            //Data to view 
            //Current Key word
            ViewBag.KeyWord = Keyword;

            //Curent Filter
            ViewBag.Filter = Filter;

            //Current Order
            ViewBag.Order = Order;

            //All contact
            var lstObj = from c in db.brands select c;

            //Filter module
            switch (Filter)
            {
                case "Name":
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.brand_name.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }


                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.brand_name);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.brand_name);
                    }
                    break;
                case "Address":
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.brand_address.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.brand_address);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.brand_address);
                    }
                    break;

                default:
                    if (!string.IsNullOrEmpty(Keyword))
                    {
                        lstObj = from c in lstObj where c.brand_name.ToUpper().Contains(Keyword.ToUpper()) || c.brand_address.ToUpper().Contains(Keyword.ToUpper()) select c;
                    }
                    //Order module
                    if (Order != null && Order == "desc")
                    {
                        lstObj = lstObj.OrderByDescending(m => m.brand_id);
                    }
                    else
                    {
                        lstObj = lstObj.OrderBy(m => m.brand_id);
                    }
                    break;
            }


            //Render Index View
            return View(lstObj.ToPagedList(Pagenumber, PageSize));
        }

        //
        // GET: /Admin/Brands/Details/5

        public ActionResult Details(int id = 0)
        {
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(new BrandModel()
            {
                brand_id = id,
                brand_address = brand.brand_address,
                brand_descripttion = brand.brand_descripttion,
                brand_fax = brand.brand_fax,
                brand_phone = brand.brand_phone,
                brand_name = brand.brand_name
            });
        }

        //
        // GET: /Admin/Brands/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Brands/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BrandModel brand)
        {
            if (ModelState.IsValid)
            {
                db.brands.Add(new brand()
                {
                    brand_address = brand.brand_address,
                    brand_descripttion = brand.brand_descripttion,
                    brand_fax = brand.brand_fax,
                    brand_name = brand.brand_name,
                    brand_phone = brand.brand_phone
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brand);
        }

        //
        // GET: /Admin/Brands/Edit/5

        public ActionResult Edit(int id = 0)
        {
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(new BrandModel()
            {
                brand_name = brand.brand_name,
                brand_phone = brand.brand_phone,
                brand_fax = brand.brand_fax,
                brand_descripttion = brand.brand_descripttion,
                brand_address = brand.brand_address,
                brand_id = brand.brand_id
            });
        }

        //
        // POST: /Admin/Brands/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BrandModel brand)
        {
            if (ModelState.IsValid)
            {
                var b = db.brands.Find(brand.brand_id);
                b.brand_phone = brand.brand_phone;
                b.brand_name = brand.brand_name;
                b.brand_fax = brand.brand_fax;
                b.brand_descripttion = brand.brand_descripttion;
                b.brand_address = brand.brand_address;
                db.Entry(b).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brand);
        }

        //
        // GET: /Admin/Brands/Delete/5

        public ActionResult Delete(int id = 0)
        {
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            ViewBag.Product = brand.products.Count;
            return View(new BrandModel()
            {
                brand_id = brand.brand_id,
                brand_address = brand.brand_address,
                brand_descripttion = brand.brand_descripttion,
                brand_fax = brand.brand_fax,
                brand_phone = brand.brand_phone,
                brand_name = brand.brand_name
            });
        }

        //
        // POST: /Admin/Brands/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            brand brand = db.brands.Find(id);
            if (brand.products.Count() > 0)
            {
                return HttpNotFound();   
            }
            db.brands.Remove(brand);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}