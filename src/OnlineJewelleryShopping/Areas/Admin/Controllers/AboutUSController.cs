﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineJewelleryShopping.Models;

namespace OnlineJewelleryShopping.Areas.Admin.Controllers
{
    public class AboutUSController : Controller
    {
        private OnlineJewelleryStoreEntities db = new OnlineJewelleryStoreEntities();

        //
        // GET: /Admin/AboutUS/

        public ActionResult Index()
        {
            return View(db.about_us.FirstOrDefault());
        }

        //
        // GET: /Admin/AboutUS/Edit/5

        public ActionResult Edit()
        {
            about_us about_us = db.about_us.FirstOrDefault();
            if (about_us == null)
            {
                db.about_us.Add(new
                     about_us() { about_us_conten = "No data" });
                return View(new about_us() { about_us_conten = "No data" });
            }
            return View(about_us);
        }

        //
        // POST: /Admin/AboutUS/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(about_us about_us)
        {
            if (ModelState.IsValid)
            {
                var obj = db.about_us.FirstOrDefault();
                if (obj != null)
                {
                    obj.about_us_conten = about_us.about_us_conten;
                    db.Entry(obj).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.about_us.Add(new
                     about_us() { about_us_conten = about_us.about_us_conten });
                    db.SaveChanges();
                }
            }
            return View(about_us);
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}